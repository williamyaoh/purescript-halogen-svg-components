{ pkgs ? import (import ../sources.nix {}).nixpkgs {} }:

let
  webpack = import ./npm-executables.nix {
    inherit (pkgs) mkYarnPackage;
    bins = ["webpack-cli"];
  };
in

{
  inherit webpack;
}
