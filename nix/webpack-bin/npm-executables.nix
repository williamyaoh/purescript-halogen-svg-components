{ mkYarnPackage, bins ? [] }:

mkYarnPackage rec {
  name = "parcel-bin";

  src = ./.;

  packageJSON = ./package.json;
  yarnLock = ./yarn.lock;
  yarnNix = ./yarn.nix;

  publishBinsFor = bins;
}
