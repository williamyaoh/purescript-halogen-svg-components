{ fetchurl, fetchgit, linkFarm, runCommandNoCC, gnutar }: rec {
  offline_cache = linkFarm "offline" packages;
  packages = [
    {
      name = "_discoveryjs_json_ext___json_ext_0.5.3.tgz";
      path = fetchurl {
        name = "_discoveryjs_json_ext___json_ext_0.5.3.tgz";
        url  = "https://registry.yarnpkg.com/@discoveryjs/json-ext/-/json-ext-0.5.3.tgz";
        sha1 = "90420f9f9c6d3987f176a19a7d8e764271a2f55d";
      };
    }
    {
      name = "_types_eslint_scope___eslint_scope_3.7.1.tgz";
      path = fetchurl {
        name = "_types_eslint_scope___eslint_scope_3.7.1.tgz";
        url  = "https://registry.yarnpkg.com/@types/eslint-scope/-/eslint-scope-3.7.1.tgz";
        sha1 = "8dc390a7b4f9dd9f1284629efce982e41612116e";
      };
    }
    {
      name = "_types_eslint___eslint_7.28.0.tgz";
      path = fetchurl {
        name = "_types_eslint___eslint_7.28.0.tgz";
        url  = "https://registry.yarnpkg.com/@types/eslint/-/eslint-7.28.0.tgz";
        sha1 = "7e41f2481d301c68e14f483fe10b017753ce8d5a";
      };
    }
    {
      name = "_types_estree___estree_0.0.50.tgz";
      path = fetchurl {
        name = "_types_estree___estree_0.0.50.tgz";
        url  = "https://registry.yarnpkg.com/@types/estree/-/estree-0.0.50.tgz";
        sha1 = "1e0caa9364d3fccd2931c3ed96fdbeaa5d4cca83";
      };
    }
    {
      name = "_types_estree___estree_0.0.46.tgz";
      path = fetchurl {
        name = "_types_estree___estree_0.0.46.tgz";
        url  = "https://registry.yarnpkg.com/@types/estree/-/estree-0.0.46.tgz";
        sha1 = "0fb6bfbbeabd7a30880504993369c4bf1deab1fe";
      };
    }
    {
      name = "_types_json_schema___json_schema_7.0.8.tgz";
      path = fetchurl {
        name = "_types_json_schema___json_schema_7.0.8.tgz";
        url  = "https://registry.yarnpkg.com/@types/json-schema/-/json-schema-7.0.8.tgz";
        sha1 = "edf1bf1dbf4e04413ca8e5b17b3b7d7d54b59818";
      };
    }
    {
      name = "_types_node___node_16.3.3.tgz";
      path = fetchurl {
        name = "_types_node___node_16.3.3.tgz";
        url  = "https://registry.yarnpkg.com/@types/node/-/node-16.3.3.tgz";
        sha1 = "0c30adff37bbbc7a50eb9b58fae2a504d0d88038";
      };
    }
    {
      name = "_webassemblyjs_ast___ast_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_ast___ast_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/ast/-/ast-1.11.0.tgz";
        sha1 = "a5aa679efdc9e51707a4207139da57920555961f";
      };
    }
    {
      name = "_webassemblyjs_floating_point_hex_parser___floating_point_hex_parser_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_floating_point_hex_parser___floating_point_hex_parser_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/floating-point-hex-parser/-/floating-point-hex-parser-1.11.0.tgz";
        sha1 = "34d62052f453cd43101d72eab4966a022587947c";
      };
    }
    {
      name = "_webassemblyjs_helper_api_error___helper_api_error_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_helper_api_error___helper_api_error_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/helper-api-error/-/helper-api-error-1.11.0.tgz";
        sha1 = "aaea8fb3b923f4aaa9b512ff541b013ffb68d2d4";
      };
    }
    {
      name = "_webassemblyjs_helper_buffer___helper_buffer_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_helper_buffer___helper_buffer_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/helper-buffer/-/helper-buffer-1.11.0.tgz";
        sha1 = "d026c25d175e388a7dbda9694e91e743cbe9b642";
      };
    }
    {
      name = "_webassemblyjs_helper_numbers___helper_numbers_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_helper_numbers___helper_numbers_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/helper-numbers/-/helper-numbers-1.11.0.tgz";
        sha1 = "7ab04172d54e312cc6ea4286d7d9fa27c88cd4f9";
      };
    }
    {
      name = "_webassemblyjs_helper_wasm_bytecode___helper_wasm_bytecode_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_helper_wasm_bytecode___helper_wasm_bytecode_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/helper-wasm-bytecode/-/helper-wasm-bytecode-1.11.0.tgz";
        sha1 = "85fdcda4129902fe86f81abf7e7236953ec5a4e1";
      };
    }
    {
      name = "_webassemblyjs_helper_wasm_section___helper_wasm_section_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_helper_wasm_section___helper_wasm_section_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/helper-wasm-section/-/helper-wasm-section-1.11.0.tgz";
        sha1 = "9ce2cc89300262509c801b4af113d1ca25c1a75b";
      };
    }
    {
      name = "_webassemblyjs_ieee754___ieee754_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_ieee754___ieee754_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/ieee754/-/ieee754-1.11.0.tgz";
        sha1 = "46975d583f9828f5d094ac210e219441c4e6f5cf";
      };
    }
    {
      name = "_webassemblyjs_leb128___leb128_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_leb128___leb128_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/leb128/-/leb128-1.11.0.tgz";
        sha1 = "f7353de1df38aa201cba9fb88b43f41f75ff403b";
      };
    }
    {
      name = "_webassemblyjs_utf8___utf8_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_utf8___utf8_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/utf8/-/utf8-1.11.0.tgz";
        sha1 = "86e48f959cf49e0e5091f069a709b862f5a2cadf";
      };
    }
    {
      name = "_webassemblyjs_wasm_edit___wasm_edit_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_wasm_edit___wasm_edit_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/wasm-edit/-/wasm-edit-1.11.0.tgz";
        sha1 = "ee4a5c9f677046a210542ae63897094c2027cb78";
      };
    }
    {
      name = "_webassemblyjs_wasm_gen___wasm_gen_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_wasm_gen___wasm_gen_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/wasm-gen/-/wasm-gen-1.11.0.tgz";
        sha1 = "3cdb35e70082d42a35166988dda64f24ceb97abe";
      };
    }
    {
      name = "_webassemblyjs_wasm_opt___wasm_opt_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_wasm_opt___wasm_opt_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/wasm-opt/-/wasm-opt-1.11.0.tgz";
        sha1 = "1638ae188137f4bb031f568a413cd24d32f92978";
      };
    }
    {
      name = "_webassemblyjs_wasm_parser___wasm_parser_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_wasm_parser___wasm_parser_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/wasm-parser/-/wasm-parser-1.11.0.tgz";
        sha1 = "3e680b8830d5b13d1ec86cc42f38f3d4a7700754";
      };
    }
    {
      name = "_webassemblyjs_wast_printer___wast_printer_1.11.0.tgz";
      path = fetchurl {
        name = "_webassemblyjs_wast_printer___wast_printer_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/@webassemblyjs/wast-printer/-/wast-printer-1.11.0.tgz";
        sha1 = "680d1f6a5365d6d401974a8e949e05474e1fab7e";
      };
    }
    {
      name = "_webpack_cli_configtest___configtest_1.0.4.tgz";
      path = fetchurl {
        name = "_webpack_cli_configtest___configtest_1.0.4.tgz";
        url  = "https://registry.yarnpkg.com/@webpack-cli/configtest/-/configtest-1.0.4.tgz";
        sha1 = "f03ce6311c0883a83d04569e2c03c6238316d2aa";
      };
    }
    {
      name = "_webpack_cli_info___info_1.3.0.tgz";
      path = fetchurl {
        name = "_webpack_cli_info___info_1.3.0.tgz";
        url  = "https://registry.yarnpkg.com/@webpack-cli/info/-/info-1.3.0.tgz";
        sha1 = "9d78a31101a960997a4acd41ffd9b9300627fe2b";
      };
    }
    {
      name = "_webpack_cli_serve___serve_1.5.1.tgz";
      path = fetchurl {
        name = "_webpack_cli_serve___serve_1.5.1.tgz";
        url  = "https://registry.yarnpkg.com/@webpack-cli/serve/-/serve-1.5.1.tgz";
        sha1 = "b5fde2f0f79c1e120307c415a4c1d5eb15a6f278";
      };
    }
    {
      name = "_xtuc_ieee754___ieee754_1.2.0.tgz";
      path = fetchurl {
        name = "_xtuc_ieee754___ieee754_1.2.0.tgz";
        url  = "https://registry.yarnpkg.com/@xtuc/ieee754/-/ieee754-1.2.0.tgz";
        sha1 = "eef014a3145ae477a1cbc00cd1e552336dceb790";
      };
    }
    {
      name = "_xtuc_long___long_4.2.2.tgz";
      path = fetchurl {
        name = "_xtuc_long___long_4.2.2.tgz";
        url  = "https://registry.yarnpkg.com/@xtuc/long/-/long-4.2.2.tgz";
        sha1 = "d291c6a4e97989b5c61d9acf396ae4fe133a718d";
      };
    }
    {
      name = "acorn___acorn_8.4.1.tgz";
      path = fetchurl {
        name = "acorn___acorn_8.4.1.tgz";
        url  = "https://registry.yarnpkg.com/acorn/-/acorn-8.4.1.tgz";
        sha1 = "56c36251fc7cabc7096adc18f05afe814321a28c";
      };
    }
    {
      name = "ajv_keywords___ajv_keywords_3.5.2.tgz";
      path = fetchurl {
        name = "ajv_keywords___ajv_keywords_3.5.2.tgz";
        url  = "https://registry.yarnpkg.com/ajv-keywords/-/ajv-keywords-3.5.2.tgz";
        sha1 = "31f29da5ab6e00d1c2d329acf7b5929614d5014d";
      };
    }
    {
      name = "ajv___ajv_6.12.6.tgz";
      path = fetchurl {
        name = "ajv___ajv_6.12.6.tgz";
        url  = "https://registry.yarnpkg.com/ajv/-/ajv-6.12.6.tgz";
        sha1 = "baf5a62e802b07d977034586f8c3baf5adf26df4";
      };
    }
    {
      name = "ansi_colors___ansi_colors_4.1.1.tgz";
      path = fetchurl {
        name = "ansi_colors___ansi_colors_4.1.1.tgz";
        url  = "https://registry.yarnpkg.com/ansi-colors/-/ansi-colors-4.1.1.tgz";
        sha1 = "cbb9ae256bf750af1eab344f229aa27fe94ba348";
      };
    }
    {
      name = "browserslist___browserslist_4.16.6.tgz";
      path = fetchurl {
        name = "browserslist___browserslist_4.16.6.tgz";
        url  = "https://registry.yarnpkg.com/browserslist/-/browserslist-4.16.6.tgz";
        sha1 = "d7901277a5a88e554ed305b183ec9b0c08f66fa2";
      };
    }
    {
      name = "buffer_from___buffer_from_1.1.1.tgz";
      path = fetchurl {
        name = "buffer_from___buffer_from_1.1.1.tgz";
        url  = "https://registry.yarnpkg.com/buffer-from/-/buffer-from-1.1.1.tgz";
        sha1 = "32713bc028f75c02fdb710d7c7bcec1f2c6070ef";
      };
    }
    {
      name = "caniuse_lite___caniuse_lite_1.0.30001245.tgz";
      path = fetchurl {
        name = "caniuse_lite___caniuse_lite_1.0.30001245.tgz";
        url  = "https://registry.yarnpkg.com/caniuse-lite/-/caniuse-lite-1.0.30001245.tgz";
        sha1 = "45b941bbd833cb0fa53861ff2bae746b3c6ca5d4";
      };
    }
    {
      name = "chrome_trace_event___chrome_trace_event_1.0.3.tgz";
      path = fetchurl {
        name = "chrome_trace_event___chrome_trace_event_1.0.3.tgz";
        url  = "https://registry.yarnpkg.com/chrome-trace-event/-/chrome-trace-event-1.0.3.tgz";
        sha1 = "1015eced4741e15d06664a957dbbf50d041e26ac";
      };
    }
    {
      name = "clone_deep___clone_deep_4.0.1.tgz";
      path = fetchurl {
        name = "clone_deep___clone_deep_4.0.1.tgz";
        url  = "https://registry.yarnpkg.com/clone-deep/-/clone-deep-4.0.1.tgz";
        sha1 = "c19fd9bdbbf85942b4fd979c84dcf7d5f07c2387";
      };
    }
    {
      name = "colorette___colorette_1.2.2.tgz";
      path = fetchurl {
        name = "colorette___colorette_1.2.2.tgz";
        url  = "https://registry.yarnpkg.com/colorette/-/colorette-1.2.2.tgz";
        sha1 = "cbcc79d5e99caea2dbf10eb3a26fd8b3e6acfa94";
      };
    }
    {
      name = "commander___commander_2.20.3.tgz";
      path = fetchurl {
        name = "commander___commander_2.20.3.tgz";
        url  = "https://registry.yarnpkg.com/commander/-/commander-2.20.3.tgz";
        sha1 = "fd485e84c03eb4881c20722ba48035e8531aeb33";
      };
    }
    {
      name = "commander___commander_7.2.0.tgz";
      path = fetchurl {
        name = "commander___commander_7.2.0.tgz";
        url  = "https://registry.yarnpkg.com/commander/-/commander-7.2.0.tgz";
        sha1 = "a36cb57d0b501ce108e4d20559a150a391d97ab7";
      };
    }
    {
      name = "cross_spawn___cross_spawn_7.0.3.tgz";
      path = fetchurl {
        name = "cross_spawn___cross_spawn_7.0.3.tgz";
        url  = "https://registry.yarnpkg.com/cross-spawn/-/cross-spawn-7.0.3.tgz";
        sha1 = "f73a85b9d5d41d045551c177e2882d4ac85728a6";
      };
    }
    {
      name = "electron_to_chromium___electron_to_chromium_1.3.779.tgz";
      path = fetchurl {
        name = "electron_to_chromium___electron_to_chromium_1.3.779.tgz";
        url  = "https://registry.yarnpkg.com/electron-to-chromium/-/electron-to-chromium-1.3.779.tgz";
        sha1 = "de55492a756deec63424f89fbe62aec9776f0e6d";
      };
    }
    {
      name = "enhanced_resolve___enhanced_resolve_5.8.2.tgz";
      path = fetchurl {
        name = "enhanced_resolve___enhanced_resolve_5.8.2.tgz";
        url  = "https://registry.yarnpkg.com/enhanced-resolve/-/enhanced-resolve-5.8.2.tgz";
        sha1 = "15ddc779345cbb73e97c611cd00c01c1e7bf4d8b";
      };
    }
    {
      name = "enquirer___enquirer_2.3.6.tgz";
      path = fetchurl {
        name = "enquirer___enquirer_2.3.6.tgz";
        url  = "https://registry.yarnpkg.com/enquirer/-/enquirer-2.3.6.tgz";
        sha1 = "2a7fe5dd634a1e4125a975ec994ff5456dc3734d";
      };
    }
    {
      name = "envinfo___envinfo_7.8.1.tgz";
      path = fetchurl {
        name = "envinfo___envinfo_7.8.1.tgz";
        url  = "https://registry.yarnpkg.com/envinfo/-/envinfo-7.8.1.tgz";
        sha1 = "06377e3e5f4d379fea7ac592d5ad8927e0c4d475";
      };
    }
    {
      name = "es_module_lexer___es_module_lexer_0.4.1.tgz";
      path = fetchurl {
        name = "es_module_lexer___es_module_lexer_0.4.1.tgz";
        url  = "https://registry.yarnpkg.com/es-module-lexer/-/es-module-lexer-0.4.1.tgz";
        sha1 = "dda8c6a14d8f340a24e34331e0fab0cb50438e0e";
      };
    }
    {
      name = "escalade___escalade_3.1.1.tgz";
      path = fetchurl {
        name = "escalade___escalade_3.1.1.tgz";
        url  = "https://registry.yarnpkg.com/escalade/-/escalade-3.1.1.tgz";
        sha1 = "d8cfdc7000965c5a0174b4a82eaa5c0552742e40";
      };
    }
    {
      name = "eslint_scope___eslint_scope_5.1.1.tgz";
      path = fetchurl {
        name = "eslint_scope___eslint_scope_5.1.1.tgz";
        url  = "https://registry.yarnpkg.com/eslint-scope/-/eslint-scope-5.1.1.tgz";
        sha1 = "e786e59a66cb92b3f6c1fb0d508aab174848f48c";
      };
    }
    {
      name = "esrecurse___esrecurse_4.3.0.tgz";
      path = fetchurl {
        name = "esrecurse___esrecurse_4.3.0.tgz";
        url  = "https://registry.yarnpkg.com/esrecurse/-/esrecurse-4.3.0.tgz";
        sha1 = "7ad7964d679abb28bee72cec63758b1c5d2c9921";
      };
    }
    {
      name = "estraverse___estraverse_4.3.0.tgz";
      path = fetchurl {
        name = "estraverse___estraverse_4.3.0.tgz";
        url  = "https://registry.yarnpkg.com/estraverse/-/estraverse-4.3.0.tgz";
        sha1 = "398ad3f3c5a24948be7725e83d11a7de28cdbd1d";
      };
    }
    {
      name = "estraverse___estraverse_5.2.0.tgz";
      path = fetchurl {
        name = "estraverse___estraverse_5.2.0.tgz";
        url  = "https://registry.yarnpkg.com/estraverse/-/estraverse-5.2.0.tgz";
        sha1 = "307df42547e6cc7324d3cf03c155d5cdb8c53880";
      };
    }
    {
      name = "events___events_3.3.0.tgz";
      path = fetchurl {
        name = "events___events_3.3.0.tgz";
        url  = "https://registry.yarnpkg.com/events/-/events-3.3.0.tgz";
        sha1 = "31a95ad0a924e2d2c419a813aeb2c4e878ea7400";
      };
    }
    {
      name = "execa___execa_5.1.1.tgz";
      path = fetchurl {
        name = "execa___execa_5.1.1.tgz";
        url  = "https://registry.yarnpkg.com/execa/-/execa-5.1.1.tgz";
        sha1 = "f80ad9cbf4298f7bd1d4c9555c21e93741c411dd";
      };
    }
    {
      name = "fast_deep_equal___fast_deep_equal_3.1.3.tgz";
      path = fetchurl {
        name = "fast_deep_equal___fast_deep_equal_3.1.3.tgz";
        url  = "https://registry.yarnpkg.com/fast-deep-equal/-/fast-deep-equal-3.1.3.tgz";
        sha1 = "3a7d56b559d6cbc3eb512325244e619a65c6c525";
      };
    }
    {
      name = "fast_json_stable_stringify___fast_json_stable_stringify_2.1.0.tgz";
      path = fetchurl {
        name = "fast_json_stable_stringify___fast_json_stable_stringify_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/fast-json-stable-stringify/-/fast-json-stable-stringify-2.1.0.tgz";
        sha1 = "874bf69c6f404c2b5d99c481341399fd55892633";
      };
    }
    {
      name = "fastest_levenshtein___fastest_levenshtein_1.0.12.tgz";
      path = fetchurl {
        name = "fastest_levenshtein___fastest_levenshtein_1.0.12.tgz";
        url  = "https://registry.yarnpkg.com/fastest-levenshtein/-/fastest-levenshtein-1.0.12.tgz";
        sha1 = "9990f7d3a88cc5a9ffd1f1745745251700d497e2";
      };
    }
    {
      name = "find_up___find_up_4.1.0.tgz";
      path = fetchurl {
        name = "find_up___find_up_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/find-up/-/find-up-4.1.0.tgz";
        sha1 = "97afe7d6cdc0bc5928584b7c8d7b16e8a9aa5d19";
      };
    }
    {
      name = "function_bind___function_bind_1.1.1.tgz";
      path = fetchurl {
        name = "function_bind___function_bind_1.1.1.tgz";
        url  = "https://registry.yarnpkg.com/function-bind/-/function-bind-1.1.1.tgz";
        sha1 = "a56899d3ea3c9bab874bb9773b7c5ede92f4895d";
      };
    }
    {
      name = "get_stream___get_stream_6.0.1.tgz";
      path = fetchurl {
        name = "get_stream___get_stream_6.0.1.tgz";
        url  = "https://registry.yarnpkg.com/get-stream/-/get-stream-6.0.1.tgz";
        sha1 = "a262d8eef67aced57c2852ad6167526a43cbf7b7";
      };
    }
    {
      name = "glob_to_regexp___glob_to_regexp_0.4.1.tgz";
      path = fetchurl {
        name = "glob_to_regexp___glob_to_regexp_0.4.1.tgz";
        url  = "https://registry.yarnpkg.com/glob-to-regexp/-/glob-to-regexp-0.4.1.tgz";
        sha1 = "c75297087c851b9a578bd217dd59a92f59fe546e";
      };
    }
    {
      name = "graceful_fs___graceful_fs_4.2.6.tgz";
      path = fetchurl {
        name = "graceful_fs___graceful_fs_4.2.6.tgz";
        url  = "https://registry.yarnpkg.com/graceful-fs/-/graceful-fs-4.2.6.tgz";
        sha1 = "ff040b2b0853b23c3d31027523706f1885d76bee";
      };
    }
    {
      name = "has_flag___has_flag_4.0.0.tgz";
      path = fetchurl {
        name = "has_flag___has_flag_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/has-flag/-/has-flag-4.0.0.tgz";
        sha1 = "944771fd9c81c81265c4d6941860da06bb59479b";
      };
    }
    {
      name = "has___has_1.0.3.tgz";
      path = fetchurl {
        name = "has___has_1.0.3.tgz";
        url  = "https://registry.yarnpkg.com/has/-/has-1.0.3.tgz";
        sha1 = "722d7cbfc1f6aa8241f16dd814e011e1f41e8796";
      };
    }
    {
      name = "human_signals___human_signals_2.1.0.tgz";
      path = fetchurl {
        name = "human_signals___human_signals_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/human-signals/-/human-signals-2.1.0.tgz";
        sha1 = "dc91fcba42e4d06e4abaed33b3e7a3c02f514ea0";
      };
    }
    {
      name = "import_local___import_local_3.0.2.tgz";
      path = fetchurl {
        name = "import_local___import_local_3.0.2.tgz";
        url  = "https://registry.yarnpkg.com/import-local/-/import-local-3.0.2.tgz";
        sha1 = "a8cfd0431d1de4a2199703d003e3e62364fa6db6";
      };
    }
    {
      name = "interpret___interpret_2.2.0.tgz";
      path = fetchurl {
        name = "interpret___interpret_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/interpret/-/interpret-2.2.0.tgz";
        sha1 = "1a78a0b5965c40a5416d007ad6f50ad27c417df9";
      };
    }
    {
      name = "is_core_module___is_core_module_2.5.0.tgz";
      path = fetchurl {
        name = "is_core_module___is_core_module_2.5.0.tgz";
        url  = "https://registry.yarnpkg.com/is-core-module/-/is-core-module-2.5.0.tgz";
        sha1 = "f754843617c70bfd29b7bd87327400cda5c18491";
      };
    }
    {
      name = "is_plain_object___is_plain_object_2.0.4.tgz";
      path = fetchurl {
        name = "is_plain_object___is_plain_object_2.0.4.tgz";
        url  = "https://registry.yarnpkg.com/is-plain-object/-/is-plain-object-2.0.4.tgz";
        sha1 = "2c163b3fafb1b606d9d17928f05c2a1c38e07677";
      };
    }
    {
      name = "is_stream___is_stream_2.0.0.tgz";
      path = fetchurl {
        name = "is_stream___is_stream_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-stream/-/is-stream-2.0.0.tgz";
        sha1 = "bde9c32680d6fae04129d6ac9d921ce7815f78e3";
      };
    }
    {
      name = "isexe___isexe_2.0.0.tgz";
      path = fetchurl {
        name = "isexe___isexe_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/isexe/-/isexe-2.0.0.tgz";
        sha1 = "e8fbf374dc556ff8947a10dcb0572d633f2cfa10";
      };
    }
    {
      name = "isobject___isobject_3.0.1.tgz";
      path = fetchurl {
        name = "isobject___isobject_3.0.1.tgz";
        url  = "https://registry.yarnpkg.com/isobject/-/isobject-3.0.1.tgz";
        sha1 = "4e431e92b11a9731636aa1f9c8d1ccbcfdab78df";
      };
    }
    {
      name = "jest_worker___jest_worker_27.0.6.tgz";
      path = fetchurl {
        name = "jest_worker___jest_worker_27.0.6.tgz";
        url  = "https://registry.yarnpkg.com/jest-worker/-/jest-worker-27.0.6.tgz";
        sha1 = "a5fdb1e14ad34eb228cfe162d9f729cdbfa28aed";
      };
    }
    {
      name = "json_parse_better_errors___json_parse_better_errors_1.0.2.tgz";
      path = fetchurl {
        name = "json_parse_better_errors___json_parse_better_errors_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/json-parse-better-errors/-/json-parse-better-errors-1.0.2.tgz";
        sha1 = "bb867cfb3450e69107c131d1c514bab3dc8bcaa9";
      };
    }
    {
      name = "json_schema_traverse___json_schema_traverse_0.4.1.tgz";
      path = fetchurl {
        name = "json_schema_traverse___json_schema_traverse_0.4.1.tgz";
        url  = "https://registry.yarnpkg.com/json-schema-traverse/-/json-schema-traverse-0.4.1.tgz";
        sha1 = "69f6a87d9513ab8bb8fe63bdb0979c448e684660";
      };
    }
    {
      name = "kind_of___kind_of_6.0.3.tgz";
      path = fetchurl {
        name = "kind_of___kind_of_6.0.3.tgz";
        url  = "https://registry.yarnpkg.com/kind-of/-/kind-of-6.0.3.tgz";
        sha1 = "07c05034a6c349fa06e24fa35aa76db4580ce4dd";
      };
    }
    {
      name = "loader_runner___loader_runner_4.2.0.tgz";
      path = fetchurl {
        name = "loader_runner___loader_runner_4.2.0.tgz";
        url  = "https://registry.yarnpkg.com/loader-runner/-/loader-runner-4.2.0.tgz";
        sha1 = "d7022380d66d14c5fb1d496b89864ebcfd478384";
      };
    }
    {
      name = "locate_path___locate_path_5.0.0.tgz";
      path = fetchurl {
        name = "locate_path___locate_path_5.0.0.tgz";
        url  = "https://registry.yarnpkg.com/locate-path/-/locate-path-5.0.0.tgz";
        sha1 = "1afba396afd676a6d42504d0a67a3a7eb9f62aa0";
      };
    }
    {
      name = "merge_stream___merge_stream_2.0.0.tgz";
      path = fetchurl {
        name = "merge_stream___merge_stream_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/merge-stream/-/merge-stream-2.0.0.tgz";
        sha1 = "52823629a14dd00c9770fb6ad47dc6310f2c1f60";
      };
    }
    {
      name = "mime_db___mime_db_1.48.0.tgz";
      path = fetchurl {
        name = "mime_db___mime_db_1.48.0.tgz";
        url  = "https://registry.yarnpkg.com/mime-db/-/mime-db-1.48.0.tgz";
        sha1 = "e35b31045dd7eada3aaad537ed88a33afbef2d1d";
      };
    }
    {
      name = "mime_types___mime_types_2.1.31.tgz";
      path = fetchurl {
        name = "mime_types___mime_types_2.1.31.tgz";
        url  = "https://registry.yarnpkg.com/mime-types/-/mime-types-2.1.31.tgz";
        sha1 = "a00d76b74317c61f9c2db2218b8e9f8e9c5c9e6b";
      };
    }
    {
      name = "mimic_fn___mimic_fn_2.1.0.tgz";
      path = fetchurl {
        name = "mimic_fn___mimic_fn_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/mimic-fn/-/mimic-fn-2.1.0.tgz";
        sha1 = "7ed2c2ccccaf84d3ffcb7a69b57711fc2083401b";
      };
    }
    {
      name = "neo_async___neo_async_2.6.2.tgz";
      path = fetchurl {
        name = "neo_async___neo_async_2.6.2.tgz";
        url  = "https://registry.yarnpkg.com/neo-async/-/neo-async-2.6.2.tgz";
        sha1 = "b4aafb93e3aeb2d8174ca53cf163ab7d7308305f";
      };
    }
    {
      name = "node_releases___node_releases_1.1.73.tgz";
      path = fetchurl {
        name = "node_releases___node_releases_1.1.73.tgz";
        url  = "https://registry.yarnpkg.com/node-releases/-/node-releases-1.1.73.tgz";
        sha1 = "dd4e81ddd5277ff846b80b52bb40c49edf7a7b20";
      };
    }
    {
      name = "npm_run_path___npm_run_path_4.0.1.tgz";
      path = fetchurl {
        name = "npm_run_path___npm_run_path_4.0.1.tgz";
        url  = "https://registry.yarnpkg.com/npm-run-path/-/npm-run-path-4.0.1.tgz";
        sha1 = "b7ecd1e5ed53da8e37a55e1c2269e0b97ed748ea";
      };
    }
    {
      name = "onetime___onetime_5.1.2.tgz";
      path = fetchurl {
        name = "onetime___onetime_5.1.2.tgz";
        url  = "https://registry.yarnpkg.com/onetime/-/onetime-5.1.2.tgz";
        sha1 = "d0e96ebb56b07476df1dd9c4806e5237985ca45e";
      };
    }
    {
      name = "p_limit___p_limit_2.3.0.tgz";
      path = fetchurl {
        name = "p_limit___p_limit_2.3.0.tgz";
        url  = "https://registry.yarnpkg.com/p-limit/-/p-limit-2.3.0.tgz";
        sha1 = "3dd33c647a214fdfffd835933eb086da0dc21db1";
      };
    }
    {
      name = "p_limit___p_limit_3.1.0.tgz";
      path = fetchurl {
        name = "p_limit___p_limit_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/p-limit/-/p-limit-3.1.0.tgz";
        sha1 = "e1daccbe78d0d1388ca18c64fea38e3e57e3706b";
      };
    }
    {
      name = "p_locate___p_locate_4.1.0.tgz";
      path = fetchurl {
        name = "p_locate___p_locate_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/p-locate/-/p-locate-4.1.0.tgz";
        sha1 = "a3428bb7088b3a60292f66919278b7c297ad4f07";
      };
    }
    {
      name = "p_try___p_try_2.2.0.tgz";
      path = fetchurl {
        name = "p_try___p_try_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/p-try/-/p-try-2.2.0.tgz";
        sha1 = "cb2868540e313d61de58fafbe35ce9004d5540e6";
      };
    }
    {
      name = "path_exists___path_exists_4.0.0.tgz";
      path = fetchurl {
        name = "path_exists___path_exists_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/path-exists/-/path-exists-4.0.0.tgz";
        sha1 = "513bdbe2d3b95d7762e8c1137efa195c6c61b5b3";
      };
    }
    {
      name = "path_key___path_key_3.1.1.tgz";
      path = fetchurl {
        name = "path_key___path_key_3.1.1.tgz";
        url  = "https://registry.yarnpkg.com/path-key/-/path-key-3.1.1.tgz";
        sha1 = "581f6ade658cbba65a0d3380de7753295054f375";
      };
    }
    {
      name = "path_parse___path_parse_1.0.7.tgz";
      path = fetchurl {
        name = "path_parse___path_parse_1.0.7.tgz";
        url  = "https://registry.yarnpkg.com/path-parse/-/path-parse-1.0.7.tgz";
        sha1 = "fbc114b60ca42b30d9daf5858e4bd68bbedb6735";
      };
    }
    {
      name = "pkg_dir___pkg_dir_4.2.0.tgz";
      path = fetchurl {
        name = "pkg_dir___pkg_dir_4.2.0.tgz";
        url  = "https://registry.yarnpkg.com/pkg-dir/-/pkg-dir-4.2.0.tgz";
        sha1 = "f099133df7ede422e81d1d8448270eeb3e4261f3";
      };
    }
    {
      name = "punycode___punycode_2.1.1.tgz";
      path = fetchurl {
        name = "punycode___punycode_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/punycode/-/punycode-2.1.1.tgz";
        sha1 = "b58b010ac40c22c5657616c8d2c2c02c7bf479ec";
      };
    }
    {
      name = "randombytes___randombytes_2.1.0.tgz";
      path = fetchurl {
        name = "randombytes___randombytes_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/randombytes/-/randombytes-2.1.0.tgz";
        sha1 = "df6f84372f0270dc65cdf6291349ab7a473d4f2a";
      };
    }
    {
      name = "rechoir___rechoir_0.7.0.tgz";
      path = fetchurl {
        name = "rechoir___rechoir_0.7.0.tgz";
        url  = "https://registry.yarnpkg.com/rechoir/-/rechoir-0.7.0.tgz";
        sha1 = "32650fd52c21ab252aa5d65b19310441c7e03aca";
      };
    }
    {
      name = "resolve_cwd___resolve_cwd_3.0.0.tgz";
      path = fetchurl {
        name = "resolve_cwd___resolve_cwd_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/resolve-cwd/-/resolve-cwd-3.0.0.tgz";
        sha1 = "0f0075f1bb2544766cf73ba6a6e2adfebcb13f2d";
      };
    }
    {
      name = "resolve_from___resolve_from_5.0.0.tgz";
      path = fetchurl {
        name = "resolve_from___resolve_from_5.0.0.tgz";
        url  = "https://registry.yarnpkg.com/resolve-from/-/resolve-from-5.0.0.tgz";
        sha1 = "c35225843df8f776df21c57557bc087e9dfdfc69";
      };
    }
    {
      name = "resolve___resolve_1.20.0.tgz";
      path = fetchurl {
        name = "resolve___resolve_1.20.0.tgz";
        url  = "https://registry.yarnpkg.com/resolve/-/resolve-1.20.0.tgz";
        sha1 = "629a013fb3f70755d6f0b7935cc1c2c5378b1975";
      };
    }
    {
      name = "safe_buffer___safe_buffer_5.2.1.tgz";
      path = fetchurl {
        name = "safe_buffer___safe_buffer_5.2.1.tgz";
        url  = "https://registry.yarnpkg.com/safe-buffer/-/safe-buffer-5.2.1.tgz";
        sha1 = "1eaf9fa9bdb1fdd4ec75f58f9cdb4e6b7827eec6";
      };
    }
    {
      name = "schema_utils___schema_utils_3.1.0.tgz";
      path = fetchurl {
        name = "schema_utils___schema_utils_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/schema-utils/-/schema-utils-3.1.0.tgz";
        sha1 = "95986eb604f66daadeed56e379bfe7a7f963cdb9";
      };
    }
    {
      name = "serialize_javascript___serialize_javascript_6.0.0.tgz";
      path = fetchurl {
        name = "serialize_javascript___serialize_javascript_6.0.0.tgz";
        url  = "https://registry.yarnpkg.com/serialize-javascript/-/serialize-javascript-6.0.0.tgz";
        sha1 = "efae5d88f45d7924141da8b5c3a7a7e663fefeb8";
      };
    }
    {
      name = "shallow_clone___shallow_clone_3.0.1.tgz";
      path = fetchurl {
        name = "shallow_clone___shallow_clone_3.0.1.tgz";
        url  = "https://registry.yarnpkg.com/shallow-clone/-/shallow-clone-3.0.1.tgz";
        sha1 = "8f2981ad92531f55035b01fb230769a40e02efa3";
      };
    }
    {
      name = "shebang_command___shebang_command_2.0.0.tgz";
      path = fetchurl {
        name = "shebang_command___shebang_command_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/shebang-command/-/shebang-command-2.0.0.tgz";
        sha1 = "ccd0af4f8835fbdc265b82461aaf0c36663f34ea";
      };
    }
    {
      name = "shebang_regex___shebang_regex_3.0.0.tgz";
      path = fetchurl {
        name = "shebang_regex___shebang_regex_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/shebang-regex/-/shebang-regex-3.0.0.tgz";
        sha1 = "ae16f1644d873ecad843b0307b143362d4c42172";
      };
    }
    {
      name = "signal_exit___signal_exit_3.0.3.tgz";
      path = fetchurl {
        name = "signal_exit___signal_exit_3.0.3.tgz";
        url  = "https://registry.yarnpkg.com/signal-exit/-/signal-exit-3.0.3.tgz";
        sha1 = "a1410c2edd8f077b08b4e253c8eacfcaf057461c";
      };
    }
    {
      name = "source_list_map___source_list_map_2.0.1.tgz";
      path = fetchurl {
        name = "source_list_map___source_list_map_2.0.1.tgz";
        url  = "https://registry.yarnpkg.com/source-list-map/-/source-list-map-2.0.1.tgz";
        sha1 = "3993bd873bfc48479cca9ea3a547835c7c154b34";
      };
    }
    {
      name = "source_map_support___source_map_support_0.5.19.tgz";
      path = fetchurl {
        name = "source_map_support___source_map_support_0.5.19.tgz";
        url  = "https://registry.yarnpkg.com/source-map-support/-/source-map-support-0.5.19.tgz";
        sha1 = "a98b62f86dcaf4f67399648c085291ab9e8fed61";
      };
    }
    {
      name = "source_map___source_map_0.6.1.tgz";
      path = fetchurl {
        name = "source_map___source_map_0.6.1.tgz";
        url  = "https://registry.yarnpkg.com/source-map/-/source-map-0.6.1.tgz";
        sha1 = "74722af32e9614e9c287a8d0bbde48b5e2f1a263";
      };
    }
    {
      name = "source_map___source_map_0.7.3.tgz";
      path = fetchurl {
        name = "source_map___source_map_0.7.3.tgz";
        url  = "https://registry.yarnpkg.com/source-map/-/source-map-0.7.3.tgz";
        sha1 = "5302f8169031735226544092e64981f751750383";
      };
    }
    {
      name = "strip_final_newline___strip_final_newline_2.0.0.tgz";
      path = fetchurl {
        name = "strip_final_newline___strip_final_newline_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/strip-final-newline/-/strip-final-newline-2.0.0.tgz";
        sha1 = "89b852fb2fcbe936f6f4b3187afb0a12c1ab58ad";
      };
    }
    {
      name = "supports_color___supports_color_8.1.1.tgz";
      path = fetchurl {
        name = "supports_color___supports_color_8.1.1.tgz";
        url  = "https://registry.yarnpkg.com/supports-color/-/supports-color-8.1.1.tgz";
        sha1 = "cd6fc17e28500cff56c1b86c0a7fd4a54a73005c";
      };
    }
    {
      name = "tapable___tapable_2.2.0.tgz";
      path = fetchurl {
        name = "tapable___tapable_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/tapable/-/tapable-2.2.0.tgz";
        sha1 = "5c373d281d9c672848213d0e037d1c4165ab426b";
      };
    }
    {
      name = "terser_webpack_plugin___terser_webpack_plugin_5.1.4.tgz";
      path = fetchurl {
        name = "terser_webpack_plugin___terser_webpack_plugin_5.1.4.tgz";
        url  = "https://registry.yarnpkg.com/terser-webpack-plugin/-/terser-webpack-plugin-5.1.4.tgz";
        sha1 = "c369cf8a47aa9922bd0d8a94fe3d3da11a7678a1";
      };
    }
    {
      name = "terser___terser_5.7.1.tgz";
      path = fetchurl {
        name = "terser___terser_5.7.1.tgz";
        url  = "https://registry.yarnpkg.com/terser/-/terser-5.7.1.tgz";
        sha1 = "2dc7a61009b66bb638305cb2a824763b116bf784";
      };
    }
    {
      name = "uri_js___uri_js_4.4.1.tgz";
      path = fetchurl {
        name = "uri_js___uri_js_4.4.1.tgz";
        url  = "https://registry.yarnpkg.com/uri-js/-/uri-js-4.4.1.tgz";
        sha1 = "9b1a52595225859e55f669d928f88c6c57f2a77e";
      };
    }
    {
      name = "v8_compile_cache___v8_compile_cache_2.3.0.tgz";
      path = fetchurl {
        name = "v8_compile_cache___v8_compile_cache_2.3.0.tgz";
        url  = "https://registry.yarnpkg.com/v8-compile-cache/-/v8-compile-cache-2.3.0.tgz";
        sha1 = "2de19618c66dc247dcfb6f99338035d8245a2cee";
      };
    }
    {
      name = "watchpack___watchpack_2.2.0.tgz";
      path = fetchurl {
        name = "watchpack___watchpack_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/watchpack/-/watchpack-2.2.0.tgz";
        sha1 = "47d78f5415fe550ecd740f99fe2882323a58b1ce";
      };
    }
    {
      name = "webpack_cli___webpack_cli_4.6.0.tgz";
      path = fetchurl {
        name = "webpack_cli___webpack_cli_4.6.0.tgz";
        url  = "https://registry.yarnpkg.com/webpack-cli/-/webpack-cli-4.6.0.tgz";
        sha1 = "27ae86bfaec0cf393fcfd58abdc5a229ad32fd16";
      };
    }
    {
      name = "webpack_merge___webpack_merge_5.8.0.tgz";
      path = fetchurl {
        name = "webpack_merge___webpack_merge_5.8.0.tgz";
        url  = "https://registry.yarnpkg.com/webpack-merge/-/webpack-merge-5.8.0.tgz";
        sha1 = "2b39dbf22af87776ad744c390223731d30a68f61";
      };
    }
    {
      name = "webpack_sources___webpack_sources_2.3.0.tgz";
      path = fetchurl {
        name = "webpack_sources___webpack_sources_2.3.0.tgz";
        url  = "https://registry.yarnpkg.com/webpack-sources/-/webpack-sources-2.3.0.tgz";
        sha1 = "9ed2de69b25143a4c18847586ad9eccb19278cfa";
      };
    }
    {
      name = "webpack___webpack_5.30.0.tgz";
      path = fetchurl {
        name = "webpack___webpack_5.30.0.tgz";
        url  = "https://registry.yarnpkg.com/webpack/-/webpack-5.30.0.tgz";
        sha1 = "07d87c182a060e0c2491062f3dc0edc85a29d884";
      };
    }
    {
      name = "which___which_2.0.2.tgz";
      path = fetchurl {
        name = "which___which_2.0.2.tgz";
        url  = "https://registry.yarnpkg.com/which/-/which-2.0.2.tgz";
        sha1 = "7c6a8dd0a636a0327e10b59c9286eee93f3f51b1";
      };
    }
    {
      name = "wildcard___wildcard_2.0.0.tgz";
      path = fetchurl {
        name = "wildcard___wildcard_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/wildcard/-/wildcard-2.0.0.tgz";
        sha1 = "a77d20e5200c6faaac979e4b3aadc7b3dd7f8fec";
      };
    }
    {
      name = "yocto_queue___yocto_queue_0.1.0.tgz";
      path = fetchurl {
        name = "yocto_queue___yocto_queue_0.1.0.tgz";
        url  = "https://registry.yarnpkg.com/yocto-queue/-/yocto-queue-0.1.0.tgz";
        sha1 = "0294eb3dee05028d31ee1a5fa2c556a6aaf10a1b";
      };
    }
  ];
}
