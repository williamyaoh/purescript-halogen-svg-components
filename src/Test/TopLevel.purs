module Test.TopLevel
  ( component
  )
where

import Prelude

import Data.Maybe ( Maybe(..) )
import Type.Proxy ( Proxy(..) )
import Effect.Aff.Class ( class MonadAff )
import Halogen as H
import Halogen.HTML as HH
import Halogen.Query as HQ
import Halogen.SVG.Elements as HSE
import Halogen.SVG.Properties as HSP
import Halogen.SVG.Component as HS
import Test.Component1 as Component1

data Action
  = Initialize

type Slots = ( "component1" :: forall q. HS.SVGSlot q Void Unit )

component :: forall q i o m. MonadAff m => H.Component q i o m
component = H.mkComponent
  { initialState: const unit
  , render
  , eval: H.mkEval $ H.defaultEval
    { initialize = Just Initialize
    , handleAction = handleAction
    }
  }

handleAction :: forall state output m. MonadAff m => Action -> H.HalogenM state Action Slots output m Unit
handleAction = case _ of
  Initialize ->
    void $ HQ.request (Proxy :: _ "component1") unit $ HS.SVGRerender { x: 0, y: 0, width: 400 }

render :: forall m action. MonadAff m => Unit -> H.ComponentHTML action Slots m
render _ =
  HH.div_
    [ HSE.svg [ HSP.width 1200.0, HSP.height 600.0, HSP.viewBox (-400.0) (-300.0) 1200.0 600.0 ]
      [ HH.slot (Proxy :: _ "component1") unit Component1.component unit absurd
      ]
    ]
