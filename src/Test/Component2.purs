module Test.Component2
  ( component )
where

import Prelude

import Halogen.SVG.Component.AST ( AST(..), FontInfo )
import Data.Tuple ( Tuple(..) )
import Effect.Class ( class MonadEffect )

import Halogen.SVG.Component as HS
import Halogen as H

type Slots :: forall k. Row k
type Slots = ()

fontInfo :: FontInfo
fontInfo = Tuple 18 "DM Mono"

component :: forall q i o m. HS.SVGComponent q i o m
component = HS.mkSVGComponent
  { initialState: const unit
  , render
  , eval: H.mkEval $ H.defaultEval
  }

render :: forall m action. MonadEffect m => Unit -> AST action Slots m
render _ =  Rect []
  [ Text [] fontInfo "Some line from Component2"
  ]
