module Test.Component1
  ( component
  )
where

import Prelude

import Halogen.SVG.Component.AST ( AST(..), FontInfo )
import Data.Int ( toNumber )
import Data.Tuple ( Tuple(..) )
import Effect.Class ( class MonadEffect )
import Type.Proxy ( Proxy(..) )

import Halogen.SVG.Component as HS
import Halogen.SVG.Elements as HSE
import Halogen.SVG.Properties as HSP
import Halogen as H
import Test.Component2 as Component2

type Slots = ( "component2" :: forall q. HS.SVGSlot q Void Unit )

fontInfo :: FontInfo
fontInfo = Tuple 18 "DM Mono"

component :: forall q i o m. HS.SVGComponent q i o m
component = HS.mkSVGComponent
  { initialState: const unit
  , render
  , eval: H.mkEval $ H.defaultEval
  }

render :: forall m action. MonadEffect m => Unit -> AST action Slots m
render _ = Rect []
  [ Text [] fontInfo "Here is some first line from Component1"
  , ManualSVG \{ bounds } ->
      { dims: { width: bounds.width, height: 50 }
      , dom: HSE.rect' [ HSP.x $ toNumber bounds.x, HSP.y $ toNumber bounds.y, HSP.width $ toNumber bounds.width, HSP.height 50.0, HSP.stroke "#000", HSP.fillOpacity 0.0 ]
      }
  , HS.slot (Proxy :: _ "component2") unit Component2.component unit absurd
  , Text [] fontInfo "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mauris odio, porta gravida facilisis at, tincidunt quis augue. Donec mollis sapien aliquet odio viverra tincidunt. Sed maximus turpis sollicitudin lorem ullamcorper gravida."
  , Text [] fontInfo "Here is some third line from Component1"
  ]
  # ManualSVGAbsolute \{ bounds, dims } ->
      { dom: HSE.rect' [ HSP.height 25.0, HSP.width 25.0, HSP.fill "#ff0000", HSP.x $ toNumber (bounds.x + dims.width) - 12.5, HSP.y $ toNumber (bounds.y + dims.height) - 12.5 ]
      }
