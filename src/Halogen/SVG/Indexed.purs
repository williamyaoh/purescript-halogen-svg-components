-- |
-- We're very deliberately implementing the SVG 1.1 spec.
-- We also avoid implementing anything marked as deprecated.
--
-- Many useful things are in the SVG 2.0 spec (like ARIA, and z-indices),
-- but unfortunately since 2.0 isn't necessarily supported by all browsers,
-- we don't want to rely on them.
--
-- See <https://www.w3.org/TR/SVG11/>

module Halogen.SVG.Indexed where

import Web.Event.Event ( Event )
import Web.UIEvent.MouseEvent ( MouseEvent )
import Web.UIEvent.FocusEvent ( FocusEvent )

-- The nomenclature here is not our own. It's pulled directly from the
-- SVG 1.1 spec.

type ConditionalProcessingAttributes r =
  ( "requiredExtensions" :: String
  , "systemLanguage" :: String
  | r
  )

type CoreAttributes r =
  ( "id" :: String
  , "xml:lang" :: String
  | r
  )

type DocumentEventAttributes r =
  ( "onAbort" :: Event
  , "onError" :: Event
  , "onZoom" :: Event
  , "onScroll" :: Event
  , "onLoad" :: Event
  , "onUnload" :: Event
  | r
  )

type GraphicalEventAttributes r =
  ( "onFocusIn" :: FocusEvent, "onFocusOut" :: FocusEvent
  , "onActivate" :: Event

  , "onClick" :: MouseEvent
  , "onMouseDown" :: MouseEvent, "onMouseUp" :: MouseEvent
  , "onMouseOver" :: MouseEvent, "onMouseOut" :: MouseEvent
  , "onMouseMove" :: MouseEvent

  , "onLoad" :: Event
  | r
  )

type StyleAttributes r =
  ( "class" :: String
  , "style" :: String
  | r
  )

type PresentationAttributes r =
  ( "alignmentBaseline" :: String
  , "baselineShift" :: String
  , "clip" :: String
  , "clipPath" :: String
  , "clipRule" :: String
  , "color" :: String
  , "colorInterpolation" :: String
  , "colorInterpolationFilters" :: String
  , "colorProfile" :: String
  , "colorRendering" :: String
  , "cursor" :: String
  , "direction" :: String
  , "display" :: String
  , "dominantBaseline" :: String
  , "enableBackground" :: String
  , "fill" :: String
  , "fillOpacity" :: Number
  , "fillRule" :: String
  , "filter" :: String
  , "floodColor" :: String
  , "floodOpacity" :: Number
  , "fontFamily" :: String
  , "fontSize" :: String
  , "fontSizeAdjust" :: Number
  , "fontStretch" :: String
  , "fontStyle" :: String
  , "fontVariant" :: String
  , "fontWeight" :: String
  , "imageRendering" :: String
  , "letterSpacing" :: String
  , "lightingColor" :: String
  , "markerEnd" :: String
  , "markerMid" :: String
  , "marketStart" :: String
  , "mask" :: String
  , "opacity" :: Number
  , "overflow" :: String
  , "pointerEvents" :: String
  , "shapeRendering" :: String
  , "stopColor" :: String
  , "stopOpacity" :: Number
  , "stroke" :: String
  , "strokeDashArray" :: String
  , "strokeDashOffset" :: String
  , "strokeLineCap" :: String
  , "strokeLineJoin" :: String
  , "strokeMiterLimit" :: Number
  , "strokeOpacity" :: Number
  , "strokeWidth" :: Number
  , "textAnchor" :: String
  , "textDecoration" :: String
  , "textRendering" :: String
  , "unicodeBidi" :: String
  , "visibility" :: String
  , "wordSpacing" :: String
  , "writingMode" :: String
  | r
  )

type AnimationEventAttributes r =
  ( "onBegin" :: Event, "onEnd" :: Event
  , "onRepeat" :: Event
  | r
  )

type AnimationAttributeTargetAttributes r =
  ( "attributeName" :: String
  | r
  )

type AnimationTimingAttributes r =
  ( "begin" :: String
  , "dur" :: String
  , "end" :: String
  , "min" :: String
  , "max" :: String
  , "restart" :: String
  , "repeatCount" :: String
  , "repeatDur" :: String
  , "fill" :: String
    -- !! This collides with "fill" from PresentationAttributes, and technically has
    -- !! a different type! The SVG 1.1 spec doesn't resolve this ambiguity either,
    -- !! so I guess it's a big middle finger.
  | r
  )

type AnimationValueAttributes r =
  ( "calcMode" :: String
  , "values" :: String
  , "keyTimes" :: String
  , "keySplines" :: String
  , "from" :: String
  , "to" :: String
  , "by" :: String
  | r
  )

type AnimationAdditionAttributes r =
  ( "additive" :: String
  , "accumulate" :: String
  | r
  )

type FilterPrimitiveAttributes r =
  ( "x" :: Number
  , "y" :: Number
  , "width" :: Number
  , "height" :: Number
  , "result" :: String
  | r
  )

type TransferFunctionElementAttributes r =
  ( "type" :: String
  , "tableValues" :: String
  , "intercept" :: Number
  , "amplitude" :: Number
  , "exponent" :: Number
  , "offset" :: String
  | r
  )

type SVGa =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "xlink:href" :: String
      -- This is the one place where we're going to add something from SVG 2.
      -- *Technically* xlink:href is deprecated, which means that there would
      -- literally be no way to use an SVG <a> element!
      , "href" :: String
      , "target" :: String
      )))))

type SVGanimate =
  ConditionalProcessingAttributes (CoreAttributes (PresentationAttributes
    (AnimationEventAttributes (AnimationAttributeTargetAttributes
      (AnimationTimingAttributes (AnimationValueAttributes
        (AnimationAdditionAttributes
          ())))))))

type SVGanimateMotion =
  ConditionalProcessingAttributes (CoreAttributes (AnimationEventAttributes
    (AnimationTimingAttributes (AnimationValueAttributes (AnimationAdditionAttributes
      ( "path" :: String
      , "keyPoints" :: String
      , "rotate" :: String
      ))))))

type SVGanimateTransform =
  ConditionalProcessingAttributes (CoreAttributes (AnimationEventAttributes
    (AnimationAttributeTargetAttributes (AnimationTimingAttributes
      (AnimationValueAttributes (AnimationAdditionAttributes
        ( "type" :: String
        )))))))

type SVGcircle =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "cx" :: Number
      , "cy" :: Number
      , "r" :: Number
      )))))

type SVGclipPath =
  ConditionalProcessingAttributes (CoreAttributes (PresentationAttributes
    (StyleAttributes
      ( "transform" :: String
      , "clipPathUnits" :: String
      ))))

type SVGdefs =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      )))))

type SVGdesc =
  CoreAttributes (StyleAttributes ())

type SVGellipse =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "cx" :: Number
      , "cy" :: Number
      , "rx" :: Number
      , "ry" :: Number
      )))))

type SVGfeBlend =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "in2" :: String
      , "mode" :: String
      ))))

type SVGfeColorMatrix =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "type" :: String
      , "values" :: String
      ))))

type SVGfeComponentTransfer =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      ))))

type SVGfeComposite =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "in2" :: String
      , "operator" :: String
      , "k1" :: Number
      , "k2" :: Number
      , "k3" :: Number
      , "k4" :: Number
      ))))

type SVGfeConvolveMatrix =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "order" :: String
      , "kernelMatrix" :: String
      , "divisor" :: Number
      , "bias" :: Number
      , "targetX" :: Int
      , "targetY" :: Int
      , "edgeMode" :: String
      , "preserveAlpha" :: Boolean
      ))))

type SVGfeDiffuseLighting =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "surfaceScale" :: Number
      , "diffuseConstant" :: Number
      ))))

type SVGfeDisplacementMap =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "in2" :: String
      , "scale" :: Number
      , "xChannelSelector" :: String
      , "yChannelSelector" :: String
      ))))

type SVGfeDistantLight =
  CoreAttributes
    ( "azimuth" :: Number
    , "elevation" :: Number
    )

type SVGfeFlood =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes ())))

type SVGfeFuncR =
  CoreAttributes (TransferFunctionElementAttributes ())

type SVGfeFuncG =
  CoreAttributes (TransferFunctionElementAttributes ())

type SVGfeFuncB =
  CoreAttributes (TransferFunctionElementAttributes ())

type SVGfeFuncA =
  CoreAttributes (TransferFunctionElementAttributes ())

type SVGfeGaussianBlur =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "stdDeviation" :: String
      ))))

type SVGfeImage =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "preserveAspectRatio" :: String
      -- Again, technically xlink:href is deprecated, but there's no
      -- way to use this element otherwise.
      , "xlink:href" :: String
      , "href" :: String
      ))))

type SVGfeMerge =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes ())))

type SVGfeMergeNode =
  CoreAttributes ( "in" :: String )

type SVGfeMorphology =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "operator" :: String
      , "radius" :: String
      ))))

type SVGfeOffset =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "dx" :: Number
      , "dy" :: Number
      ))))

type SVGfePointLight =
  CoreAttributes
    ( "x" :: Number
    , "y" :: Number
    , "z" :: Number
    )

type SVGfeSpecularLighting =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      , "surfaceScale" :: Number
      , "specularConstant" :: Number
      , "specularExponent" :: Number
      ))))

type SVGfeSpotlight =
  CoreAttributes
    ( "x" :: Number
    , "y" :: Number
    , "z" :: Number
    , "pointsAtX" :: Number
    , "pointsAtY" :: Number
    , "pointsAtZ" :: Number
    , "specularExponent" :: Number
    , "limitingConeAngle" :: Number
    )

type SVGfeTile =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "in" :: String
      ))))

type SVGfeTurbulence =
  CoreAttributes (PresentationAttributes (FilterPrimitiveAttributes
    (StyleAttributes
      ( "baseFrequency" :: String
      , "numOctaves" :: Int
      , "seed" :: Number
      , "stitchTiles" :: String
      , "type" :: String
      ))))

type SVGfilter =
  CoreAttributes (PresentationAttributes (StyleAttributes
    ( "x" :: Number
    , "y" :: Number
    , "width" :: Number
    , "height" :: Number
    , "filterRes" :: String
    , "filterUnits" :: String
    , "primitiveUnits" :: String
    , "xlink:href" :: String
    , "href" :: String
    )))

type SVGforeignObject =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "x" :: Number
      , "y" :: Number
      , "width" :: Number
      , "height" :: Number
      )))))

type SVGg =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      )))))

type SVGimage =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "preserveAspectRatio" :: String
      , "transform" :: String
      , "x" :: Number
      , "y" :: Number
      , "width" :: Number
      , "height" :: Number
      , "xlink:href" :: String
      , "href" :: String
      )))))

type SVGline =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "x1" :: Number
      , "y1" :: Number
      , "x2" :: Number
      , "y2" :: Number
      )))))

type SVGlinearGradient =
  CoreAttributes (PresentationAttributes (StyleAttributes
    ( "x1" :: Number
    , "y1" :: Number
    , "x2" :: Number
    , "y2" :: Number
    , "gradientUnits" :: String
    , "gradientTransform" :: String
    , "spreadMethod" :: String
    , "xlink:href" :: String
    , "href" :: String
    )))

type SVGmarker =
  CoreAttributes (PresentationAttributes (StyleAttributes
    ( "viewBox" :: String
    , "preserveAspectRatio" :: String
    , "refX" :: Number
    , "refY" :: Number
    , "markerUnits" :: String
    , "markerWidth" :: Number
    , "markerHeight" :: Number
    , "orient" :: String
    )))

type SVGmask =
  ConditionalProcessingAttributes (CoreAttributes (PresentationAttributes
    (StyleAttributes
      ( "x" :: Number
      , "y" :: Number
      , "width" :: Number
      , "height" :: Number
      , "maskUnits" :: String
      , "maskContentUnits" :: String
      ))))

type SVGmetadata =
  CoreAttributes ()

type SVGmpath =
  CoreAttributes
    ( "xlink:href" :: String
    , "href" :: String
    )

type SVGpath =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "d" :: String
      , "pathLength" :: Number
      )))))

type SVGpattern =
  ConditionalProcessingAttributes (CoreAttributes (PresentationAttributes
    (StyleAttributes
      ( "viewBox" :: String
      , "preserveAspectRatio" :: String
      , "x" :: Number
      , "y" :: Number
      , "width" :: Number
      , "height" :: Number
      , "patternUnits" :: String
      , "patternContentUnits" :: String
      , "patternTransform" :: String
      , "xlink:href" :: String
      , "href" :: String
      ))))

type SVGpolygon =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "points" :: String
      )))))

type SVGpolyline =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "points" :: String
      )))))

type SVGradialGradient =
  CoreAttributes (PresentationAttributes (StyleAttributes
    ( "cx" :: Number
    , "cy" :: Number
    , "r" :: Number
    , "fx" :: Number
    , "fy" :: Number
    , "gradientUnits" :: String
    , "gradientTransform" :: String
    , "spreadMethod" :: String
    , "xlink:href" :: String
    , "href" :: String
    )))

type SVGrect =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "x" :: Number
      , "y" :: Number
      , "width" :: Number
      , "height" :: Number
      , "rx" :: Number
      , "ry" :: Number
      )))))

type SVGscript =
  CoreAttributes
    ( "type" :: String
    , "xlink:href" :: String
    , "href" :: String
    )

type SVGset =
  ConditionalProcessingAttributes (CoreAttributes (AnimationEventAttributes
    (AnimationAttributeTargetAttributes (AnimationTimingAttributes
      ( "to" :: String
      )))))

type SVGstop =
  CoreAttributes (PresentationAttributes (StyleAttributes
    ( "offset" :: String
    )))

type SVGstyle =
  CoreAttributes
    ( "type" :: String
    , "media" :: String
    , "title" :: String
    )

type SVGsvg =
  ConditionalProcessingAttributes (CoreAttributes (DocumentEventAttributes
    (GraphicalEventAttributes (PresentationAttributes (StyleAttributes
      ( "x" :: Number
      , "y" :: Number
      , "width" :: Number
      , "height" :: Number
      , "viewBox" :: String
      , "preserveAspectRatio" :: String
      ))))))

type SVGswitch =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      )))))

type SVGsymbol =
  CoreAttributes (GraphicalEventAttributes (PresentationAttributes
    (StyleAttributes
      ( "preserveAspectRatio" :: String
      , "viewBox" :: String
      ))))

type SVGtext =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "lengthAdjust" :: String
      -- *Technically* for <text>, these are all lists of scalar values.
      -- But I don't know when we'd ever use that.
      , "x" :: Number
      , "y" :: Number
      , "dx" :: Number
      , "dy" :: Number
      , "rotate" :: String
      , "textLength" :: String
      )))))

type SVGtextPath =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "xlink:href" :: String
      , "href" :: String
      , "startOffset" :: Number
      , "method" :: String
      , "spacing" :: String
      )))))

type SVGtitle =
  CoreAttributes (StyleAttributes ())

type SVGtspan =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "x" :: Number
      , "y" :: Number
      , "dx" :: Number
      , "dy" :: Number
      , "rotate" :: Number
      , "textLength" :: Number
      , "lengthAdjust" :: String
      )))))

type SVGuse =
  ConditionalProcessingAttributes (CoreAttributes (GraphicalEventAttributes
    (PresentationAttributes (StyleAttributes
      ( "transform" :: String
      , "x" :: Number
      , "y" :: Number
      , "width" :: Number
      , "height" :: Number
      , "xlink:href" :: String
      , "href" :: String
      )))))

type SVGview =
  CoreAttributes
    ( "viewBox" :: String
    , "preserveAspectRatio" :: String
    )
