module Halogen.SVG.Component
  ( SVGComponent
  , SVGComponentSpec
  , SVGSlot
  , SVGQuery(..)
  , mkSVGComponent
  , slot
  , tell
  , tellAll
  , request
  , requestAll
  , module X
    -- * Implementation types and function that you likely won't need to look at
  , focusState
  , transformHalogenF
  , toHalogenComponent
  )
where

import Prelude

import Control.Monad.Free ( Free )
import Data.Lens ( Lens', view, set, over )
import Data.Lens.Record ( prop )
import Data.Map ( Map )
import Data.Maybe ( Maybe(..), fromMaybe, maybe )
import Data.Symbol ( class IsSymbol )
import Data.Tuple ( Tuple(..) )
import Effect.Class ( class MonadEffect, liftEffect )
import Effect.Class.Console ( error )
import Effect.Ref ( Ref )
import Effect.Unsafe ( unsafePerformEffect )
import Halogen.Component ( ComponentSlot(..), componentSlot )
import Halogen.Query.HalogenM ( HalogenM(..), HalogenF(..), HalogenAp(..) )
import Halogen.Query.HalogenQ ( HalogenQ(..) )
import Halogen.SVG.Component.AST ( Dimensions, DisplayBounds, AST, TextualCache, emptyTextualCache )
import Halogen.SVG.Component.Automaton ( Automaton )
import Prim.Row ( class Cons )
import Safe.Coerce ( coerce )
import Type.Proxy ( Proxy(..) )
import Unsafe.Reference ( unsafeRefEq )

import Control.Applicative.Free as FreeAp
import Control.Monad.Free as Free
import Control.Monad.State.Class as State
import Data.Coyoneda as Coyoneda
import Effect.Ref as Ref
import Halogen as H
import Halogen.HTML as HH
import Halogen.Query as HQ
import Halogen.SVG.Component.AST as AST
import Halogen.SVG.Component.Automaton as Automaton

import Halogen.Query ( Tell, Request, mkTell, mkRequest ) as X

-- | A wrapper around each component's specific query type, so we can ensure
-- | that all SVG components have the ability to respond to requests from their
-- | parents to rerender with specific parameters.
data SVGQuery q a
  = SVGRerender DisplayBounds (Dimensions -> a)
  | BaseQuery (q a)

derive instance functorSVGQuery :: Functor q => Functor (SVGQuery q)

type SVGComponent query input output m =
  MonadEffect m => H.Component (SVGQuery query) input output m

type SVGComponentSpec state query action slots input output m =
  { initialState :: input -> state
  , render :: state -> AST action slots m
  , eval :: HalogenQ query action input ~> HalogenM state action slots output m
  }

type SVGSlot query output slot =
  H.Slot (SVGQuery query) output slot

-- | Extra information needed for an SVG component to render its AST properly.
type SVGCache action slots m =
  { displayCache :: Maybe (Ref TextualCache)
  , displayBounds :: Maybe DisplayBounds
  , vdom :: Maybe (H.ComponentHTML action slots m)
  }

type WithSVGCache state action slots m =
  { state :: state
  , svgCache :: SVGCache action slots m
  }

emptySVGCache :: forall action slots m. SVGCache action slots m
emptySVGCache =
  { displayCache: Nothing
  , displayBounds: Nothing
  , vdom: Nothing
  }

focusState
  :: forall state state' action slots output m a
   . Lens' state state'
  -> HalogenM state' action slots output m a
  -> HalogenM state action slots output m a
focusState lens (HalogenM free) =
  HalogenM $ Free.hoistFree (transformHalogenF lens) free

transformHalogenF
  :: forall state state' action slots output m a
   . Lens' state state'
  -> HalogenF state' action slots output m a
  -> HalogenF state action slots output m a
transformHalogenF lens = case _ of
  State f -> State \state -> map (\new -> set lens new state) $ f (view lens state)
  Subscribe f g -> Subscribe f g
  Unsubscribe subID x -> Unsubscribe subID x
  Lift ma -> Lift ma
  ChildQuery box -> ChildQuery box
  Raise o x -> Raise o x
  Par (HalogenAp freeap) -> Par (coerce (FreeAp.hoistFreeAp (focusState lens) freeap))
  Fork runner f -> Fork (focusState lens runner) f
  Join forkID x -> Join forkID x
  Kill forkID x -> Kill forkID x
  GetRef label k -> GetRef label k

-- | Ensure that the VDom cache gets updated when the underlying state changes.
hookStateSVGRender
  :: forall state action slots output m a
   . MonadEffect m
  => (state -> AST action slots m)
  -> HalogenM (WithSVGCache state action slots m) action slots output m a
  -> HalogenM (WithSVGCache state action slots m) action slots output m a
hookStateSVGRender renderF =
  coerce <<< Free.substFree go <<< coerce
  where
    go
      :: forall b
       . HalogenF (WithSVGCache state action slots m) action slots output m b
      -> Free (HalogenF (WithSVGCache state action slots m) action slots output m) b
    go = case _ of
      State f -> do
        { rerender, result } <- Free.liftF $ State \state ->
          let Tuple result state' = f state
          in Tuple { rerender: not (state `unsafeRefEq` state'), result } state'
        when rerender $ (\(HalogenM x) -> x) $ void $ renderSVG renderF
        pure result
      other -> Free.liftF other

-- | Rerender the VDom attribute in the SVG cache, based on the current
-- | display bounds. Return the calculated dimensions.
renderSVG
  :: forall state action slots output m
   . MonadEffect m
  => (state -> AST action slots m)
  -> HalogenM (WithSVGCache state action slots m) action slots output m Dimensions
renderSVG renderF = do
  { svgCache, state } <- State.get
  case svgCache.displayBounds of
    -- TODO: Not sure what the best approach here is to make sure there's always
    -- TODO: a display bounds. Probably hooking things into the input to ensure
    -- TODO: it's there.
    Nothing -> do
      error "attempted to render before display bounds were available"
      pure { width: -1, height: -1 }
    Just bounds -> do
      uninitChildren <- liftEffect $ Ref.new false

      let ast = renderF state

      -- There's a slight problem when we try to query subcomponents for their
      -- dimensions for the first time, namely that Halogen hasn't initialized
      -- them yet, and *won't do so until we update the stored VDom*. That won't
      -- happen until we've already passed the point where we needed to know the
      -- child's rendered dimensions!
      --
      -- So our simple, slightly hackish solution around this is to check if any
      -- of the rerender queries return Nothing (indicating that the subcomponent
      -- hasn't initialized yet), and render ourselves *twice* if that's the
      -- case.
      --
      -- Note that in the "happy path" where all children are already initialized,
      -- this causes no extra renders, so we should only pay this extra cost rarely.
      dims <- render uninitChildren bounds ast
      liftEffect (Ref.read uninitChildren) >>= if _
        then render uninitChildren bounds ast
        else pure dims
  where
    render
      :: Ref Boolean
      -> DisplayBounds
      -> AST action slots m
      -> HalogenM (WithSVGCache state action slots m) action slots output m Dimensions
    render uc bounds ast = do
      { svgCache } <- State.get
      oldCache <- liftEffect $ maybe (Ref.new emptyTextualCache) pure svgCache.displayCache
      newCache <- liftEffect $ Ref.new emptyTextualCache

      -- This usage of `unsafePerformEffect' is safe because renderTextWithCaches
      -- only uses the refs for caching; they do not change the returned result.
      let automaton = Automaton.initAutomaton
            (unsafePerformEffect <<< AST.renderTextWithCaches oldCache newCache)
            ast
            bounds

      Tuple dims vdom <- go uc bounds automaton

      -- Now that we've rerendered, rotate the display
      -- cache and insert the calculated VDom
      State.modify_ $ over
        (prop (Proxy :: _ "svgCache"))
        (_ { displayCache = Just newCache, vdom = Just vdom } )
      pure dims

    go
      :: Ref Boolean
      -> DisplayBounds
      -> Automaton action slots m
      -> HalogenM (WithSVGCache state action slots m) action slots output m (Tuple Dimensions (H.ComponentHTML action slots m))
    go uc bounds automaton = case Automaton.getOutput automaton of
      Automaton.Done _bounds dims vdom -> pure (Tuple dims vdom)
      Automaton.NeedsInput cbounds boundsF -> do
        cdims <- boundsF cbounds >>= case _ of
          Nothing -> liftEffect (Ref.write true uc) *> pure { width: 0, height: 0 }
          Just d -> pure d
        go uc bounds $ Automaton.supplyDimensions cdims automaton
      Automaton.Continue -> Automaton.step1 automaton # go uc bounds
      -- TODO: Not sure what to do in this case either. And here there's no
      -- TODO: good way to remove it unless we can somehow remove the possibility
      -- TODO: of failure from the automaton. Maybe we could possibly do that
      -- TODO: in the types?
      Automaton.Crash -> do
        error "SVG automaton crashed while rendering component"
        pure $ Tuple { width: -1, height: -1 } (HH.text "")

toHalogenComponent
  :: forall state query action slots input output m
   . MonadEffect m
  => SVGComponentSpec state query action slots input output m
  -> H.ComponentSpec
       (WithSVGCache state action slots m)
       (SVGQuery query)
       action
       slots
       input
       output
       m
toHalogenComponent svgComponent =
  { initialState: \input ->
    { state: svgComponent.initialState input
    , svgCache: emptySVGCache
    }
  , render: \state -> fromMaybe (HH.text "") state.svgCache.vdom
  , eval:
      let eval'
            :: forall b. HalogenQ query action input b
            -> HalogenM (WithSVGCache state action slots m) action slots output m b
          eval' = hookStateSVGRender svgComponent.render
            <<< focusState (prop (Proxy :: _ "state"))
            <$> svgComponent.eval
      in case _ of
        Initialize a -> do
          cacheRef <- liftEffect $ Ref.new emptyTextualCache
          State.modify_ $
            set (prop (Proxy :: _ "svgCache") <<< prop (Proxy :: _ "displayCache"))
                (Just cacheRef)
          eval' $ Initialize a
        Finalize a -> eval' $ Finalize a
        Receive input a -> eval' $ Receive input a
        Action action a -> eval' $ Action action a
        Query coyo default_ -> do
         Coyoneda.unCoyoneda
          (\f -> case _ of
            SVGRerender bounds k -> do
              State.modify_ $
                set (prop (Proxy :: _"svgCache") <<< prop (Proxy :: _ "displayBounds"))
                    (Just bounds)
              f <<< k <$> renderSVG svgComponent.render
            BaseQuery baseQry ->
              eval' (Query (Just <$> Coyoneda.liftCoyoneda baseQry) (const Nothing))
                <#> case _ of
                      Nothing -> default_ unit
                      Just x -> f x)
          coyo
  }

mkSVGComponent
  :: forall state query action slots input output m
   . MonadEffect m
  => SVGComponentSpec state query action slots input output m
  -> H.Component (SVGQuery query) input output m
mkSVGComponent = H.mkComponent <<< toHalogenComponent

-- | Halogen's `slot', specialized to SVG components.
slot
  :: forall query action input output slots m label slot _1
   . Cons label (H.Slot (SVGQuery query) output slot) _1 slots
  => IsSymbol label
  => Ord slot
  => Proxy label
  -> slot
  -> H.Component (SVGQuery query) input output m
  -> input
  -> (output -> action)
  -> AST action slots m
slot p label component i oF =
  AST.ChildComponent
    (ComponentSlot (componentSlot p label component i (Just <<< oF)))
    (HQ.request p label <<< SVGRerender)

-- | `tell', specialized to SVG components. Highly recommended that you use
-- | this function when trying to send queries to SVG components, whether
-- | from normal Halogen components or other SVG components.
tell
  :: forall state action output m label slots query output' slot _1
   . Cons label (H.Slot (SVGQuery query) output' slot) _1 slots
  => IsSymbol label
  => Ord slot
  => Proxy label
  -> slot
  -> HQ.Tell query
  -> HalogenM state action slots output m Unit
tell p s q =
  HQ.tell p s (map BaseQuery q)

-- | `tellAll', specialized to SVG components. Highly recommended that you use
-- | this function when trying to send queries to SVG components, whether
-- | from normal Halogen components or other SVG components.
tellAll
  :: forall state action output m label slots query output' slot _1
   . Cons label (H.Slot (SVGQuery query) output' slot) _1 slots
  => IsSymbol label
  => Ord slot
  => Proxy label
  -> HQ.Tell query
  -> HalogenM state action slots output m Unit
tellAll p q =
  HQ.tellAll p (map BaseQuery q)

-- | `request', specialized to SVG components. Highly recommended that you use
-- | this function when trying to send queries to SVG components, whether from
-- | normal Halogen components or other SVG components.
request
  :: forall state action output m label slots query output' slot a _1
   . Cons label (H.Slot (SVGQuery query) output' slot) _1 slots
  => IsSymbol label
  => Ord slot
  => Proxy label
  -> slot
  -> HQ.Request query a
  -> HalogenM state action slots output m (Maybe a)
request p s q =
  HQ.request p s (map BaseQuery q)

-- | `requestAll', specialized to SVG components. Highly recommended that you
-- | use this function when trying to send queries to SVG components, whether
-- | from normal Halogen components or other SVG components.
requestAll
  :: forall state action output m label slots query output' slot a _1
   . Cons label (H.Slot (SVGQuery query) output' slot) _1 slots
  => IsSymbol label
  => Ord slot
  => Proxy label
  -> HQ.Request query a
  -> HalogenM state action slots output m (Map slot a)
requestAll p q =
  HQ.requestAll p (map BaseQuery q)
