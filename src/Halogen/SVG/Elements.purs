-- | Note that there are many elements that you wouldn't normally think
-- | of as containing children, but which the SVG spec permits to do so,
-- | since they may need to contain descriptors. For instance, <rect> and
-- | <circle> are tags that you would usually use without child nodes, but
-- | the SVG spec permits them to have children, so you can add descriptors
-- | and metadata.
-- |
-- | Because of this, in addition to the Halogen convention that suffixing
-- | with `_' gives an element that doesn't take any attributes, we also
-- | add the convention that suffixing with a single quote denotes a version
-- | of the element that doesn't take children. So `rect' permits children,
-- | while `rect\'' does not.

module Halogen.SVG.Elements
  ( element
  , a
  , a_
  , animate
  , animate_
  , animate'
  , animateMotion
  , animateMotion_
  , animateTransform
  , animateTransform_
  , animateTransform'
  , circle
  , circle_
  , circle'
  , clipPath
  , clipPath_
  , defs
  , defs_
  , desc
  , desc_
  , ellipse
  , ellipse_
  , ellipse'
  , feBlend
  , feBlend_
  , feBlend'
  , feColorMatrix
  , feColorMatrix_
  , feColorMatrix'
  , feComponentTransfer
  , feComponentTransfer_
  , feComposite
  , feComposite_
  , feComposite'
  , feConvolveMatrix
  , feConvolveMatrix_
  , feConvolveMatrix'
  , feDiffuseLighting
  , feDiffuseLighting_
  , feDisplacementMap
  , feDisplacementMap_
  , feDisplacementMap'
  , feDistantLight
  , feDistantLight_
  , feDistantLight'
  , feFlood
  , feFlood_
  , feFlood'
  , feFuncR
  , feFuncR_
  , feFuncR'
  , feFuncG
  , feFuncG_
  , feFuncG'
  , feFuncB
  , feFuncB_
  , feFuncB'
  , feFuncA
  , feFuncA_
  , feFuncA'
  , feGaussianBlur
  , feGaussianBlur_
  , feGaussianBlur'
  , feImage
  , feImage_
  , feImage'
  , feMerge
  , feMerge_
  , feMergeNode
  , feMergeNode_
  , feMergeNode'
  , feMorphology
  , feMorphology_
  , feMorphology'
  , feOffset
  , feOffset_
  , feOffset'
  , fePointLight
  , fePointLight_
  , fePointLight'
  , feSpecularLighting
  , feSpecularLighting_
  , feSpotlight
  , feSpotlight_
  , feSpotlight'
  , feTile
  , feTile_
  , feTile'
  , feTurbulence
  , feTurbulence_
  , feTurbulence'
  , filter
  , filter_
  , foreignObject
  , foreignObject_
  , g
  , g_
  , image
  , image_
  , image'
  , line
  , line_
  , line'
  , linearGradient
  , linearGradient_
  , marker
  , marker_
  , mask
  , mask_
  , metadata
  , metadata_
  , mpath
  , mpath_
  , mpath'
  , path
  , path_
  , path'
  , pattern
  , pattern_
  , polygon
  , polygon_
  , polygon'
  , polyline
  , polyline_
  , polyline'
  , radialGradient
  , radialGradient_
  , rect
  , rect_
  , rect'
  , script
  , script_
  , set
  , set_
  , set'
  , stop
  , stop_
  , stop'
  , style
  , style_
  , svg
  , svg_
  , switch
  , switch_
  , symbol
  , symbol_
  , text
  , text_
  , textPath
  , textPath_
  , title
  , title_
  , tspan
  , tspan_
  , use
  , use_
  , use'
  , view
  , view_
  , view'
  , svgNS
  )
where

import Halogen.VDom.Types ( ElemName(..), Namespace(..) )
import Halogen.HTML.Core ( HTML )
import Halogen.HTML.Elements as HE
import Halogen.HTML.Elements ( Node, Leaf )
import Halogen.HTML.Properties ( IProp )

import Halogen.SVG.Indexed as SVG

svgNS :: Namespace
svgNS = Namespace "http://www.w3.org/2000/svg"

-- |
-- Create an SVG DOM element. This is different from creating an HTML DOM element,
-- because the namespaceURI property needs to be different.
element :: forall r w i. ElemName
        -> Array (IProp r i)
        -> Array (HTML w i)
        -> HTML w i
element = HE.elementNS svgNS

a :: forall w i. Node SVG.SVGa w i
a = element (ElemName "a")

a_ :: forall w i. Array (HTML w i) -> HTML w i
a_ = a []

animate :: forall w i. Node SVG.SVGanimate w i
animate = element (ElemName "animate")

animate_ :: forall w i. Array (HTML w i) -> HTML w i
animate_ = animate []

animate' :: forall w i. Leaf SVG.SVGanimate w i
animate' props = animate props []

animateMotion :: forall w i. Node SVG.SVGanimateMotion w i
animateMotion = element (ElemName "animateMotion")

animateMotion_ :: forall w i. Array (HTML w i) -> HTML w i
animateMotion_ = animateMotion []

animateTransform :: forall w i. Node SVG.SVGanimateTransform w i
animateTransform = element (ElemName "animateTransform")

animateTransform_ :: forall w i. Array (HTML w i) -> HTML w i
animateTransform_ = animateTransform []

animateTransform' :: forall w i. Leaf SVG.SVGanimateTransform w i
animateTransform' props = animateTransform props []

circle :: forall w i. Node SVG.SVGcircle w i
circle = element (ElemName "circle")

circle_ :: forall w i. Array (HTML w i) -> HTML w i
circle_ = circle []

circle' :: forall w i. Leaf SVG.SVGcircle w i
circle' props = circle props []

clipPath :: forall w i. Node SVG.SVGclipPath w i
clipPath = element (ElemName "clipPath")

clipPath_ :: forall w i. Array (HTML w i) -> HTML w i
clipPath_ = clipPath []

defs :: forall w i. Node SVG.SVGdefs w i
defs = element (ElemName "defs")

defs_ :: forall w i. Array (HTML w i) -> HTML w i
defs_ = defs []

desc :: forall w i. Node SVG.SVGdesc w i
desc = element (ElemName "desc")

desc_ :: forall w i. Array (HTML w i) -> HTML w i
desc_ = desc []

ellipse :: forall w i. Node SVG.SVGellipse w i
ellipse = element (ElemName "ellipse")

ellipse_ :: forall w i. Array (HTML w i) -> HTML w i
ellipse_ = ellipse []

ellipse' :: forall w i. Leaf SVG.SVGellipse w i
ellipse' props = ellipse props []

feBlend :: forall w i. Node SVG.SVGfeBlend w i
feBlend = element (ElemName "feBlend")

feBlend_ :: forall w i. Array (HTML w i) -> HTML w i
feBlend_ = feBlend []

feBlend' :: forall w i. Leaf SVG.SVGfeBlend w i
feBlend' props = feBlend props []

feColorMatrix :: forall w i. Node SVG.SVGfeColorMatrix w i
feColorMatrix = element (ElemName "feColorMatrix")

feColorMatrix_ :: forall w i. Array (HTML w i) -> HTML w i
feColorMatrix_ = feColorMatrix []

feColorMatrix' :: forall w i. Leaf SVG.SVGfeColorMatrix w i
feColorMatrix' props = feColorMatrix props []

feComponentTransfer :: forall w i. Node SVG.SVGfeComponentTransfer w i
feComponentTransfer = element (ElemName "feComponentTransfer")

feComponentTransfer_ :: forall w i. Array (HTML w i) -> HTML w i
feComponentTransfer_ = feComponentTransfer []

feComposite :: forall w i. Node SVG.SVGfeComposite w i
feComposite = element (ElemName "feComposite")

feComposite_ :: forall w i. Array (HTML w i) -> HTML w i
feComposite_ = feComposite []

feComposite' :: forall w i. Leaf SVG.SVGfeComposite w i
feComposite' props = feComposite props []

feConvolveMatrix :: forall w i. Node SVG.SVGfeConvolveMatrix w i
feConvolveMatrix = element (ElemName "feConvolveMatrix")

feConvolveMatrix_ :: forall w i. Array (HTML w i) -> HTML w i
feConvolveMatrix_ = feConvolveMatrix []

feConvolveMatrix' :: forall w i. Leaf SVG.SVGfeConvolveMatrix w i
feConvolveMatrix' props = feConvolveMatrix props []

feDiffuseLighting :: forall w i. Node SVG.SVGfeDiffuseLighting w i
feDiffuseLighting = element (ElemName "feDiffuseLighting")

feDiffuseLighting_ :: forall w i. Array (HTML w i) -> HTML w i
feDiffuseLighting_ = feDiffuseLighting []

feDisplacementMap :: forall w i. Node SVG.SVGfeDisplacementMap w i
feDisplacementMap = element (ElemName "feDisplacementMap")

feDisplacementMap_ :: forall w i. Array (HTML w i) -> HTML w i
feDisplacementMap_ = feDisplacementMap []

feDisplacementMap' :: forall w i. Leaf SVG.SVGfeDisplacementMap w i
feDisplacementMap' props = feDisplacementMap props []

feDistantLight :: forall w i. Node SVG.SVGfeDistantLight w i
feDistantLight = element (ElemName "feDistantLight")

feDistantLight_ :: forall w i. Array (HTML w i) -> HTML w i
feDistantLight_ = feDistantLight []

feDistantLight' :: forall w i. Leaf SVG.SVGfeDistantLight w i
feDistantLight' props = feDistantLight props []

feFlood :: forall w i. Node SVG.SVGfeFlood w i
feFlood = element (ElemName "feFlood")

feFlood_ :: forall w i. Array (HTML w i) -> HTML w i
feFlood_ = feFlood []

feFlood' :: forall w i. Leaf SVG.SVGfeFlood w i
feFlood' props = feFlood props []

feFuncR :: forall w i. Node SVG.SVGfeFuncR w i
feFuncR = element (ElemName "feFuncR")

feFuncR_ :: forall w i. Array (HTML w i) -> HTML w i
feFuncR_ = feFuncR []

feFuncR' :: forall w i. Leaf SVG.SVGfeFuncR w i
feFuncR' props = feFuncR props []

feFuncG :: forall w i. Node SVG.SVGfeFuncG w i
feFuncG = element (ElemName "feFuncG")

feFuncG_ :: forall w i. Array (HTML w i) -> HTML w i
feFuncG_ = feFuncG []

feFuncG' :: forall w i. Leaf SVG.SVGfeFuncG w i
feFuncG' props = feFuncG props []

feFuncB :: forall w i. Node SVG.SVGfeFuncB w i
feFuncB = element (ElemName "feFuncB")

feFuncB_ :: forall w i. Array (HTML w i) -> HTML w i
feFuncB_ = feFuncB []

feFuncB' :: forall w i. Leaf SVG.SVGfeFuncB w i
feFuncB' props = feFuncB props []

feFuncA :: forall w i. Node SVG.SVGfeFuncA w i
feFuncA = element (ElemName "feFuncA")

feFuncA_ :: forall w i. Array (HTML w i) -> HTML w i
feFuncA_ = feFuncA []

feFuncA' :: forall w i. Leaf SVG.SVGfeFuncA w i
feFuncA' props = feFuncA props []

feGaussianBlur :: forall w i. Node SVG.SVGfeGaussianBlur w i
feGaussianBlur = element (ElemName "feGaussianBlur")

feGaussianBlur_ :: forall w i. Array (HTML w i) -> HTML w i
feGaussianBlur_ = feGaussianBlur []

feGaussianBlur' :: forall w i. Leaf SVG.SVGfeGaussianBlur w i
feGaussianBlur' props = feGaussianBlur props []

feImage :: forall w i. Node SVG.SVGfeImage w i
feImage = element (ElemName "feImage")

feImage_ :: forall w i. Array (HTML w i) -> HTML w i
feImage_ = feImage []

feImage' :: forall w i. Leaf SVG.SVGfeImage w i
feImage' props = feImage props []

feMerge :: forall w i. Node SVG.SVGfeMerge w i
feMerge = element (ElemName "feMerge")

feMerge_ :: forall w i. Array (HTML w i) -> HTML w i
feMerge_ = feMerge []

feMergeNode :: forall w i. Node SVG.SVGfeMergeNode w i
feMergeNode = element (ElemName "feMergeNode")

feMergeNode_ :: forall w i. Array (HTML w i) -> HTML w i
feMergeNode_ = feMergeNode []

feMergeNode' :: forall w i. Leaf SVG.SVGfeMergeNode w i
feMergeNode' props = feMergeNode props []

feMorphology :: forall w i. Node SVG.SVGfeMorphology w i
feMorphology = element (ElemName "feMorphology")

feMorphology_ :: forall w i. Array (HTML w i) -> HTML w i
feMorphology_ = feMorphology []

feMorphology' :: forall w i. Leaf SVG.SVGfeMorphology w i
feMorphology' props = feMorphology props []

feOffset :: forall w i. Node SVG.SVGfeOffset w i
feOffset = element (ElemName "feOffset")

feOffset_ :: forall w i. Array (HTML w i) -> HTML w i
feOffset_ = feOffset []

feOffset' :: forall w i. Leaf SVG.SVGfeOffset w i
feOffset' props = feOffset props []

fePointLight :: forall w i. Node SVG.SVGfePointLight w i
fePointLight = element (ElemName "fePointLight")

fePointLight_ :: forall w i. Array (HTML w i) -> HTML w i
fePointLight_ = fePointLight []

fePointLight' :: forall w i. Leaf SVG.SVGfePointLight w i
fePointLight' props = fePointLight props []

feSpecularLighting :: forall w i. Node SVG.SVGfeSpecularLighting w i
feSpecularLighting = element (ElemName "feSpecularLighting")

feSpecularLighting_ :: forall w i. Array (HTML w i) -> HTML w i
feSpecularLighting_ = feSpecularLighting []

feSpotlight :: forall w i. Node SVG.SVGfeSpotlight w i
feSpotlight = element (ElemName "feSpotlight")

feSpotlight_ :: forall w i. Array (HTML w i) -> HTML w i
feSpotlight_ = feSpotlight []

feSpotlight' :: forall w i. Leaf SVG.SVGfeSpotlight w i
feSpotlight' props = feSpotlight props []

feTile :: forall w i. Node SVG.SVGfeTile w i
feTile = element (ElemName "feTile")

feTile_ :: forall w i. Array (HTML w i) -> HTML w i
feTile_ = feTile []

feTile' :: forall w i. Leaf SVG.SVGfeTile w i
feTile' props = feTile props []

feTurbulence :: forall w i. Node SVG.SVGfeTurbulence w i
feTurbulence = element (ElemName "feTurbulence")

feTurbulence_ :: forall w i. Array (HTML w i) -> HTML w i
feTurbulence_ = feTurbulence []

feTurbulence' :: forall w i. Leaf SVG.SVGfeTurbulence w i
feTurbulence' props = feTurbulence props []

filter :: forall w i. Node SVG.SVGfilter w i
filter = element (ElemName "filter")

filter_ :: forall w i. Array (HTML w i) -> HTML w i
filter_ = filter []

foreignObject :: forall w i. Node SVG.SVGforeignObject w i
foreignObject = element (ElemName "foreignObject")

foreignObject_ :: forall w i. Array (HTML w i) -> HTML w i
foreignObject_ = foreignObject []

g :: forall w i. Node SVG.SVGg w i
g = element (ElemName "g")

g_ :: forall w i. Array (HTML w i) -> HTML w i
g_ = g []

image :: forall w i. Node SVG.SVGimage w i
image = element (ElemName "image")

image_ :: forall w i. Array (HTML w i) -> HTML w i
image_ = image []

image' :: forall w i. Leaf SVG.SVGimage w i
image' props = image props []

line :: forall w i. Node SVG.SVGline w i
line = element (ElemName "line")

line_ :: forall w i. Array (HTML w i) -> HTML w i
line_ = line []

line' :: forall w i. Leaf SVG.SVGline w i
line' props = line props []

linearGradient :: forall w i. Node SVG.SVGlinearGradient w i
linearGradient = element (ElemName "linearGradient")

linearGradient_ :: forall w i. Array (HTML w i) -> HTML w i
linearGradient_ = linearGradient []

marker :: forall w i. Node SVG.SVGmarker w i
marker = element (ElemName "marker")

marker_ :: forall w i. Array (HTML w i) -> HTML w i
marker_ = marker []

mask :: forall w i. Node SVG.SVGmask w i
mask = element (ElemName "mask")

mask_ :: forall w i. Array (HTML w i) -> HTML w i
mask_ = mask []

metadata :: forall w i. Node SVG.SVGmetadata w i
metadata = element (ElemName "metadata")

metadata_ :: forall w i. Array (HTML w i) -> HTML w i
metadata_ = metadata []

mpath :: forall w i. Node SVG.SVGmpath w i
mpath = element (ElemName "mpath")

mpath_ :: forall w i. Array (HTML w i) -> HTML w i
mpath_ = mpath []

mpath' :: forall w i. Leaf SVG.SVGmpath w i
mpath' props = mpath props []

path :: forall w i. Node SVG.SVGpath w i
path = element (ElemName "path")

path_ :: forall w i. Array (HTML w i) -> HTML w i
path_ = path []

path' :: forall w i. Leaf SVG.SVGpath w i
path' props = path props []

pattern :: forall w i. Node SVG.SVGpattern w i
pattern = element (ElemName "pattern")

pattern_ :: forall w i. Array (HTML w i) -> HTML w i
pattern_ = pattern []

polygon :: forall w i. Node SVG.SVGpolygon w i
polygon = element (ElemName "polygon")

polygon_ :: forall w i. Array (HTML w i) -> HTML w i
polygon_ = polygon []

polygon' :: forall w i. Leaf SVG.SVGpolygon w i
polygon' props = polygon props []

polyline :: forall w i. Node SVG.SVGpolyline w i
polyline = element (ElemName "polyline")

polyline_ :: forall w i. Array (HTML w i) -> HTML w i
polyline_ = polyline []

polyline' :: forall w i. Leaf SVG.SVGpolyline w i
polyline' props = polyline props []

radialGradient :: forall w i. Node SVG.SVGradialGradient w i
radialGradient = element (ElemName "radialGradient")

radialGradient_ :: forall w i. Array (HTML w i) -> HTML w i
radialGradient_ = radialGradient []

rect :: forall w i. Node SVG.SVGrect w i
rect = element (ElemName "rect")

rect_ :: forall w i. Array (HTML w i) -> HTML w i
rect_ = rect []

rect' :: forall w i. Leaf SVG.SVGrect w i
rect' props = rect props []

script :: forall w i. Node SVG.SVGscript w i
script = element (ElemName "script")

script_ :: forall w i. Array (HTML w i) -> HTML w i
script_ = script []

set :: forall w i. Node SVG.SVGset w i
set = element (ElemName "set")

set_ :: forall w i. Array (HTML w i) -> HTML w i
set_ = set []

set' :: forall w i. Leaf SVG.SVGset w i
set' props = set props []

stop :: forall w i. Node SVG.SVGstop w i
stop = element (ElemName "stop")

stop_ :: forall w i. Array (HTML w i) -> HTML w i
stop_ = stop []

stop' :: forall w i. Leaf SVG.SVGstop w i
stop' props = stop props []

style :: forall w i. Node SVG.SVGstyle w i
style = element (ElemName "style")

style_ :: forall w i. Array (HTML w i) -> HTML w i
style_ = style []

svg :: forall w i. Node SVG.SVGsvg w i
svg = element (ElemName "svg")

svg_ :: forall w i. Array (HTML w i) -> HTML w i
svg_ = svg []

switch :: forall w i. Node SVG.SVGswitch w i
switch = element (ElemName "switch")

switch_ :: forall w i. Array (HTML w i) -> HTML w i
switch_ = switch []

symbol :: forall w i. Node SVG.SVGsymbol w i
symbol = element (ElemName "symbol")

symbol_ :: forall w i. Array (HTML w i) -> HTML w i
symbol_ = symbol []

text :: forall w i. Node SVG.SVGtext w i
text = element (ElemName "text")

text_ :: forall w i. Array (HTML w i) -> HTML w i
text_ = text []

textPath :: forall w i. Node SVG.SVGtextPath w i
textPath = element (ElemName "textPath")

textPath_ :: forall w i. Array (HTML w i) -> HTML w i
textPath_ = textPath []

title :: forall w i. Node SVG.SVGtitle w i
title = element (ElemName "title")

title_ :: forall w i. Array (HTML w i) -> HTML w i
title_ = title []

tspan :: forall w i. Node SVG.SVGtspan w i
tspan = element (ElemName "tspan")

tspan_ :: forall w i. Array (HTML w i) -> HTML w i
tspan_ = tspan []

use :: forall w i. Node SVG.SVGuse w i
use = element (ElemName "use")

use_ :: forall w i. Array (HTML w i) -> HTML w i
use_ = use []

use' :: forall w i. Leaf SVG.SVGuse w i
use' props = use props []

view :: forall w i. Node SVG.SVGview w i
view = element (ElemName "view")

view_ :: forall w i. Array (HTML w i) -> HTML w i
view_ = view []

view' :: forall w i. Leaf SVG.SVGview w i
view' props = view props []
