-- | While SVG 2.0/3.0 is an acceptable specification language for static
-- | images, it's actually a pretty terrible language for building interfaces.
-- |
-- | For instance, it lacks any idea of having one element spatially nested
-- | within another; all elements must be absolutely positioned.
-- | Text must be manually layouted and formatted.
-- |
-- | Since we want to be able to build Halogen-style components using SVG in
-- | the browser, these are all problems we need to solve. This module provides
-- | a custom AST for describing visual elements more akin to HTML's box model,
-- | while providing easy translation to the sort of virtual DOM that Halogen
-- | requires.

module Halogen.SVG.Component.AST
  ( module Halogen.SVG.Component.AST
  , module X
  )
where

import Prelude

import Data.Generic.Rep ( class Generic )
import Data.HashMap ( HashMap )
import Data.Int ( round, toNumber )
import Data.Maybe ( Maybe(..) )
import Data.Show.Generic ( genericShow )
import Data.String.Render ( Line, Overflow(..), pixelWidth, breakLines )
import Data.Symbol ( class IsSymbol )
import Data.Tuple ( Tuple(..) )
import Effect ( Effect )
import Effect.Ref ( Ref )
import Halogen.HTML.Properties ( IProp )
import Halogen.Query.HalogenM ( HalogenM )
import Halogen.SVG.Indexed ( GraphicalEventAttributes )
import Prim.Row ( class Cons )
import Record.Builder ( Builder )
import Type.Proxy ( Proxy(..) )

import Data.HashMap as HM
import Data.String.Render ( Overflow(..) ) as X
import Effect.Ref as Ref
import Halogen as H
import Record.Builder as Builder

type Dimensions = { width :: Int, height :: Int }

type DisplayBounds = { x :: Int, y :: Int, width :: Int }

-- | For each child component, we store a function to send it its updated
-- | display bounds when its parent updates.
type SupplyBoundsF action slots m =
  forall state output. DisplayBounds -> HalogenM state action slots output m (Maybe Dimensions)

newtype SparseRecord (row :: Row Type) =
  SparseRecord (Builder (Record row) (Record row))

instance semigroupSparseRecord :: Semigroup (SparseRecord r) where
  append (SparseRecord bldr1) (SparseRecord bldr2) = SparseRecord $ bldr1 >>> bldr2
instance monoidSparseRecord :: Monoid (SparseRecord r) where
  mempty = SparseRecord identity

-- | Build the sparse record into a full record, given some default fields.
buildSparse :: forall r. SparseRecord r -> Record r -> Record r
buildSparse (SparseRecord bldr) defs =
  Builder.build bldr defs

rectSparse :: SparseRecord SVGMLrect
rectSparse = SparseRecord $
  Builder.modify (Proxy :: Proxy "cursor") (const "pointer") >>>
  Builder.modify (Proxy :: Proxy "padding") (const $ uniformBox $ Pixel 5)

defaultSVGMLrect :: Record SVGMLrect
defaultSVGMLrect =
  { margin: uniformBox (Pixel 0)
  , border: { color: "#000000", width: 0, opacity: 0.0 }
  , padding: uniformBox (Pixel 0)
  , background: { color: "#000000", opacity: 0.0 }
  , borderRadius: uniformBorderRadius 0
  , cursor: "auto"
  }

defaultSVGMLtext :: Record SVGMLtext
defaultSVGMLtext =
  { margin: uniformBox (Pixel 0)
  , color: "#000000"
  , textAlign: LeftTextAlign
  , cursor: "auto"
  , lineHeight: 1.0
  , overflow: Visible
  }

defaultSVGMLimage :: Record SVGMLimage
defaultSVGMLimage =
  { margin: uniformBox (Pixel 0)
  , cursor: "auto"
  , opacity: 1.0
  , imageRendering: ImageRenderingAuto
  , href: ""
  , yRatio: 1.0
  }

prop
  :: forall proxy label a r r0
   . IsSymbol label
  => Cons label a r r0
  => proxy label
  -> a
  -> SparseRecord r0
prop _ = \x -> SparseRecord $
  Builder.modify (Proxy :: Proxy label) (const x)

margin :: forall r. Margin -> SparseRecord ( "margin" :: Margin | r )
margin = prop (Proxy :: Proxy "margin")

border :: forall r. Border -> SparseRecord ( "border" :: Border | r )
border = prop (Proxy :: Proxy "border")

padding :: forall r. Padding -> SparseRecord ( "padding" :: Padding | r )
padding = prop (Proxy :: Proxy "padding")

background :: forall r. Background -> SparseRecord ( "background" :: Background | r )
background = prop (Proxy :: Proxy "background")

borderRadius :: forall r. BorderRadius -> SparseRecord ( "borderRadius" :: BorderRadius | r )
borderRadius = prop (Proxy :: Proxy "borderRadius")

cursor :: forall r. String -> SparseRecord ( "cursor" :: String | r )
cursor = prop (Proxy :: Proxy "cursor")

color :: forall r. String -> SparseRecord ( "color" :: String | r )
color = prop (Proxy :: Proxy "color")

textAlign :: forall r. TextAlign -> SparseRecord ( "textAlign" :: TextAlign | r )
textAlign = prop (Proxy :: Proxy "textAlign")

lineHeight :: forall r. Number -> SparseRecord ( "lineHeight" :: Number | r )
lineHeight = prop (Proxy :: Proxy "lineHeight")

overflow :: forall r. Overflow -> SparseRecord ( "overflow" :: Overflow | r)
overflow = prop (Proxy :: Proxy "overflow")

opacity :: forall r. Number -> SparseRecord ( "opacity" :: Number | r )
opacity = prop (Proxy :: Proxy "opacity")

imageRendering :: forall r. ImageRendering -> SparseRecord ( "imageRendering" :: ImageRendering | r )
imageRendering = prop (Proxy :: Proxy "imageRendering")

href :: forall r. String -> SparseRecord ( "href" :: String | r )
href = prop (Proxy :: Proxy "href")

yRatio :: forall r. Number -> SparseRecord ( "yRatio" :: Number | r )
yRatio = prop (Proxy :: Proxy "yRatio")

data ASTProp dom svgml action
  = FromDOM (IProp dom action)
  | FromSVG (SparseRecord svgml)

-- | CSS font specifier, like "12px Roboto Mono". First argument is the font size.
type FontInfo = Tuple Int String

type ASTtext = GraphicalEventAttributes ()
type ASTrect = GraphicalEventAttributes ()
type ASTimage = GraphicalEventAttributes ()

type SVGMLtext =
  ( "margin" :: Margin
  , "color" :: String
  , "textAlign" :: TextAlign
  , "cursor" :: String
  , "lineHeight" :: Number
  , "overflow" :: Overflow
  )

type SVGMLrect =
  ( "margin" :: Margin
  , "border" :: Border
  , "padding" :: Padding
  , "background" :: Background
  , "borderRadius" :: BorderRadius
  -- We decline to make @cursor@ a cascading property, because cascading
  -- properties are the devil when it comes to writing encapsulated components.
  , "cursor" :: String
  )

type SVGMLimage =
  ( "margin" :: Margin
  , "cursor" :: String
  , "opacity" :: Number
  , "imageRendering" :: ImageRendering
  , "href" :: String
  , "yRatio" :: Number
  )

data ImageRendering
  = ImageRenderingAuto
  | ImageRenderingOptimizeSpeed
  | ImageRenderingOptimizeQuality

derive instance genericImageRendering :: Generic ImageRendering _
instance showImageRendering :: Show ImageRendering where
  show = genericShow

data TextAlign
  = Center
  | Justify
  | LeftTextAlign
  | RightTextAlign

derive instance genericTextAlign :: Generic TextAlign _
instance showTextAlign :: Show TextAlign where
  show = genericShow

type Border =
  { color :: String
  , width :: Int
  , opacity :: Number
  }

type Background =
  { color :: String
  , opacity :: Number
  }

data SVGMLUnit = Pixel Int | Percentage Number

derive instance genericSVGMLUnit :: Generic SVGMLUnit _
instance showSVGMLUnit :: Show SVGMLUnit where
  show = genericShow

normalizeUnit :: SVGMLUnit -> Int -> Int
normalizeUnit u vec = case u of
  Pixel x -> x
  Percentage p -> round (toNumber vec * p)

type BoxParams =
  { left :: SVGMLUnit
  , right :: SVGMLUnit
  , top :: SVGMLUnit
  , bottom :: SVGMLUnit
  }

type Margin = BoxParams
type Padding = BoxParams

uniformBox :: SVGMLUnit -> BoxParams
uniformBox u =
  { left: u, right: u, top: u, bottom: u }

oblongBox :: { vertical :: SVGMLUnit, horizontal :: SVGMLUnit } -> BoxParams
oblongBox { vertical: v, horizontal: h } =
  { left: h, right: h, top: v, bottom: v }

box :: BoxParams -> BoxParams
box = identity

-- We would like BorderRadius to use SVGMLUnit and allow percentage-based
-- radii, but right now doing so makes converting it into an actual property
-- on the DOM node difficult. Properties that don't affect the box model
-- only get converted into IProps using the data stored directly in the
-- SVGMLrect record, which means we don't have an easy way to key off the
-- width of the rectangle.
type BorderRadius =
  { x :: Int
  , y :: Int
  }

uniformBorderRadius :: Int -> BorderRadius
uniformBorderRadius u =
  { x: u, y: u }

data AST action slots m
  = Text (Array (ASTProp ASTtext SVGMLtext action)) FontInfo String  -- Display width is implied by the container
  | Rect (Array (ASTProp ASTrect SVGMLrect action)) (Array (AST action slots m))
  -- | Line
  | Image (Array (ASTProp ASTimage SVGMLimage action))
  | ChildComponent (H.ComponentSlot slots m action) (SupplyBoundsF action slots m)
  | ManualSVG ({ renderText :: RenderText, bounds :: DisplayBounds } -> { dims :: Dimensions, dom :: H.ComponentHTML action slots m })
  | ManualSVGAbsolute ({ renderText :: RenderText, bounds :: DisplayBounds, dims :: Dimensions } -> { dom :: H.ComponentHTML action slots m }) (AST action slots m)

-- `ManualSVG' and `ManualSVGAbsolute' are two different "overrides" that let
-- component authors just shove in SVG, for situations where what we currently
-- support isn't enough.
--
-- `ManualSVG' behaves like a normal layouted element; it gets passed the bounds
-- and position within which it should render, and is expected to return some
-- SVG that's within those bounds, as well as accurate information about the
-- dimensions of the manual SVG. The parent of the `ManualSVG', if any, will be
-- expanded according to the dimension returned. For obvious reasons, it's
-- recommended that you accurately report the dimensions of the element.
--
-- `ManualSVGAbsolute' instead produces an element which is removed from the
-- normal layout. In addition to the body function, it also takes in another
-- AST, to which the manual element is "attached." The body function is passed
-- the *final* dimensions of the SVG AST it's attached to, after said AST's
-- evaluation has finished. This allows the user to accurately position extra
-- visuals based on how the element actually looks; say, to put a widget at
-- the top or bottom right. The layout of the element being attached to is not
-- affected at all; think of a `position: absolute` HTML element.
-- I recommend using `ManualSVGAbsolute' with the (#) operator, to "attach"
-- to another AST value.

-- How do we allow things to set their own width rather than be defined
-- explicitly by the surrounding context? MAYBE WE DON'T. MAYBE THAT'S JUST NOT
-- A THING THEY GET TO DO.

-- | Data to calculate the display information for some text, as well as
-- | to look up that calculated information in the cache.
type TextualContext =
  { text :: String
  , fontInfo :: FontInfo
  , displayWidth :: Int
  , overflow :: Overflow
  }

type TextualCache = HashMap TextualContext (Array Line)

type RenderText = TextualContext -> Array Line

emptyTextualCache :: TextualCache
emptyTextualCache = HM.empty

-- | Expects the new cache to be empty.
renderTextWithCaches
  :: Ref TextualCache
  -> Ref TextualCache
  -> TextualContext
  -> Effect (Array Line)
renderTextWithCaches oldCache newCache context = do
  mCached <- Ref.read oldCache <#> HM.lookup context
  result <- case mCached of
    Just cached -> pure cached
    Nothing ->
      let
        Tuple fontSize fontName = context.fontInfo
        renderWord = \t -> pixelWidth t (show fontSize <> "px " <> fontName)
      in pure $ breakLines { renderWord, lineLen: toNumber context.displayWidth, overflow: context.overflow } context.text
  Ref.modify_ (HM.insert context result) newCache
  pure result
