module Halogen.SVG.Component.Automaton where

import Prelude

import Data.Array ( mapMaybe )
import Data.Foldable ( fold )
import Data.Int ( round, toNumber )
import Data.Lens ( over )
import Data.Lens.Record ( prop )
import Data.Lens.Setter.Argument ( arg )
import Data.List ( List(..) )
import Data.Maybe ( Maybe(..) )
import Data.String.Render ( Line )
import Data.Tuple ( Tuple(..) )
import Halogen ( ComponentHTML, ComponentSlot )
import Halogen.HTML.Core ( widget )
import Halogen.HTML.Properties ( IProp, expand )
import Halogen.SVG.Component.AST
  ( Dimensions
  , DisplayBounds
  , SupplyBoundsF
  , AST(..)
  , ASTProp(..)
  , ASTtext
  , ASTrect
  , ASTimage
  , SVGMLtext
  , SVGMLrect
  , SVGMLimage
  , FontInfo
  , RenderText
  , SparseRecord(..)
  , BoxParams
  , TextAlign(..)
  , ImageRendering(..)
  , defaultSVGMLrect
  , defaultSVGMLtext
  , defaultSVGMLimage
  , normalizeUnit
  )
import Halogen.SVG.Indexed ( SVGtext, SVGtspan, SVGg, SVGrect, SVGimage )
import Type.Proxy ( Proxy(..) )

import Data.Array as Array
import Data.List as List
import Halogen.HTML as HH
import Halogen.SVG.Elements as HSE
import Halogen.SVG.Properties as HSP
import Record.Builder as Builder

-- | # How the automaton is meant to be used
-- |
-- | This automaton generates the Halogen VDOM needed. Other than that, however,
-- | it's built to be entirely pure and not interact with Halogen at all. That
-- | way we're only doing things related to calculating the layout and not
-- | adding extra complexity from dealing with Halogen state.
-- |
-- | So none of the code to actually display the VDOM is in this module. Instead,
-- | some other effectful driver will interact with Halogen, calling into this
-- | to convert an SVG AST to VDOM. Because it's an automaton, it's not a simple
-- | function call; the driver will 1) step the automaton, 2) check the current
-- | output of the automaton, 3) provide the automaton any information it needs,
-- | 4) repeat.
-- |
-- | The `Continue`, `Done`, and `Crash` constructors are pretty straightforward.
-- | `NeedsInput` is a bit more mystifying, but essentially the automaton emits
-- | it when it has reached a point where it needs the dimensions of a child SVG
-- | component. Because the automaton is *pure*, there's nothing it can do to
-- | fetch those dimensions; rendering the child component requires running
-- | things in HalogenM. Hence it chucks it back to the driver to figure out;
-- | once the driver has rendered the child component, control is passed back to
-- | the automaton using `supplyDimensions`. The `SupplyBoundsF` is a function
-- | in HalogenM that rerenders the child for the driver's convenience.
data Output action slots m
  = Continue
  | Done DisplayBounds Dimensions (ComponentHTML action slots m)
  | NeedsInput DisplayBounds (SupplyBoundsF action slots m)
  | Crash

instance showOutput :: Show (Output action slots m) where
  show = case _ of
    Continue -> "Continue"
    (Done bounds dims _) -> "Done " <> show bounds <> " " <> show dims
    (NeedsInput _ _) -> "NeedsInput"
    Crash -> "Crash"

-- | A CPS-style output function for evaluating subtrees. To see why this is
-- | useful, consider evaluating a @Rect@ subtree: in order to output the
-- | @<rect>@ in the DOM, we need the dimensions of all the rectangle's children.
-- | And to properly group everything together under a @<g>@, we need access to
-- | the calculated VDOM nodes of the children as well. The dimensions of the
-- | output and the children outputted are inextricably linked, so we need both
-- | pieces of information when finalizing a subtree.
type EvalSubtreeF action slots m =
  { children :: List (ComponentHTML action slots m)
  , childDims :: Dimensions  -- ^ The parent's dimensions, minus its box model i.e. without margin, border, and padding
  } ->
    { dom :: ComponentHTML action slots m
    , dims :: Dimensions
    , bounds :: DisplayBounds
    }

-- | # About the automaton
-- |
-- | An automaton for incrementally calculating the dimensions of an SVG
-- | tree, based on the dimensions of its children.
-- |
-- | To see why we need this, consider how we'd need to calculate the display
-- | attributes of a parent SVG component with multiple child SVG components.
-- | If we have an opaque child component, we don't know its dimensions until
-- | we render it; not only that, but all of its sibling nodes after it depend
-- | on its dimensions as well. But in order for the child to render itself,
-- | /it/ needs information from the parent about where to place itself and
-- | what bounds it can render in, information that the parent has to calculate
-- | from the SVG tree and siblings /before/ the child. So we have an annoying
-- | dependency graph of things constantly needing display information back
-- | and forth, which we can't satisfy with a simple tree traversal. So instead
-- | we need to incrementally calculate it, stopping and getting information from
-- | each child as we encounter them, and feed the information the child gives
-- | back to us, back into the algorithm.
type Automaton action slots m =
  { stack :: Frame action slots m
  , renderText :: RenderText
  }

type EvaluateSubtreesArgs action slots m =
  { children :: List (AST action slots m)
      -- List of children to evaluate. Elements get stripped off as the tree gets evaluated.
  , childBounds :: DisplayBounds
      -- Bounds that the children must be placed within.
  , childDims :: Dimensions
      -- Calculated dimensions of the children. Accumulates the dimensions of each child.
  , evalF :: EvalSubtreeF action slots m
      -- Finalizer to produce the single VDom node, total dimensions, and bounds for the whole subtree.
  , next :: Frame action slots m
  }

type EvaluateAbsoluteArgs action slots m =
  { bodyF :: { renderText :: RenderText, bounds :: DisplayBounds, dims :: Dimensions } -> { dom :: ComponentHTML action slots m }
  , next :: Frame action slots m
  }

-- TODO: We should really turn these into records with actual field names.
-- TODO: SupplyDisplayBounds and Evaluate always occur together. Should we just
--       combine them?

-- | # Flow of the automaton
-- |
-- | The two main frame types are `Evaluate` and `CalculatedDimensions`. The
-- | goal is to get from an `Evaluate` (containing a value of the SVG AST), to
-- | a `CalculatedDimensions` (containing the Halogen VDOM and the calculated
-- | dimensions of the SVG). This process is recursive: every node of the AST
-- | will eventually end up in an `Evaluate`, and its `CalculatedDimension`
-- | will either be the final output or combine with the dimensions of its
-- | parent. Once the automaton reaches `CalculatedDimensions ... Bottom`, the
-- | conversion from SVG to VDOM is done.
-- |
-- | The rest of the frame types are there as intermediate states between
-- | `Evaluate` and `CalculatedDimensions`.
-- |
-- | `SupplyDisplayBounds` always goes with an Evaluate; it essentially gives
-- | the Evaluate the position and width the AST should render within. Height
-- | is assumed to expand indefinitely. We wrap the initial Evaluate with one,
-- | and each subelement Evaluate with one as well.
-- |
-- | `SupplyDimensions` and `EvaluateChild` go together; EvaluateChild is
-- | generated when the AST in Evaluate is a child component, and means that
-- | we need our driver to supply us some information, since we can't calculate
-- | the child dimensions ourselves. When an EvaluateChild is on top of the
-- | stack, we halt and output `NeedsInput`, and wait for our driver. The driver
-- | then calls `supplyDimensions` once it has the info, wrapping the EvaluateChild
-- | with a SupplyDimensions frame and allowing us to continue.
-- |
-- | `Bottom` is simply the bottom of the automaton stack, and tells us when to
-- | stop evaluating.
-- |
-- | `EvaluateSubtrees` is generated whenever the AST in Evaluate is a grouping
-- | element, like a <g> or <rect>. For the sequence of child AST nodes, we
-- | repeatedly break off the first child and wrap it in an Evaluate. We modify
-- | the EvaluateSubtrees frame with the tail of the children and put it
-- | underneath the new Evaluate, and then the dimensions from the eventual
-- | CalculatedDimensions gets added into the EvaluateSubtrees. Once all children
-- | have been evaluated, we call the finalizer in the EvaluateSubtrees frame
-- | to generate the grouping element, now that we know the dimensions of all
-- | the children.
-- |
-- | `EvaluateAbsolute` is specifically for evaluating `ManualSVGAbsolute`
-- | overrides; it simply holds the body function for the manual SVG to wait
-- | for the dimensions of whatever AST the override is attached to. Specifically,
-- | we construct a (Evaluate ast EvaluateAbsolute), and wait for the Evaluate
-- | to turn into a CalculatedDimensions.
-- |
-- | You can think of each stack frame like this: whatever is on top is what's
-- | currently getting evaluated/rendered, and anything below it on the stack
-- | are its ancestors that are waiting for its dimensions.
-- |
-- | # Quirks of the implementation
-- |
-- | The same `Dimensions` that get returned from the automaton as the dimensions
-- | of the element also get used for determining how much space to reserve in
-- | the parent element. On paper this seems fine, but this also includes
-- | margins! So even the dimensions that library users see when rendering will
-- | be too large for elements that have margins. This also means that if you
-- | give an element a specific width when rendering, and that element has
-- | margins, it will end up looking smaller than you'd probably expect.
-- |
-- | Other than that, dimensions work about as you'd expect from `box-sizing: border-box`.
-- | It will include the space from borders and padding.
data Frame action slots m
  = Bottom
  | Evaluate (AST action slots m) (Frame action slots m)
  | EvaluateSubtrees (EvaluateSubtreesArgs action slots m)
  | EvaluateChild
      DisplayBounds
      (ComponentSlot slots m action)
      (SupplyBoundsF action slots m)
      (Frame action slots m)
  | EvaluateAbsolute (EvaluateAbsoluteArgs action slots m)
  | SupplyDimensions Dimensions (Frame action slots m)
  | SupplyDisplayBounds DisplayBounds (Frame action slots m)
  | CalculatedDimensions
      DisplayBounds
      Dimensions
      (ComponentHTML action slots m)
      (Frame action slots m)

instance showFrame :: Show (Frame action slots m) where
  show Bottom = "Bottom"
  show (Evaluate _ast next) = "Evaluate o (" <> show next <> ")"
  show (EvaluateSubtrees { children: sub, childBounds: bounds, childDims: dims, next }) =
    "EvaluateSubtrees " <> show (map (const "o") (Array.fromFoldable sub))
      <> " " <> show bounds <> " " <> show dims <> " (" <> show next <> ")"
  show (EvaluateChild bounds _ _ next) =
    "EvaluateChild " <> show bounds <> " o (" <> show next <> ")"
  show (EvaluateAbsolute args) =
    "EvaluateAbsolute o (" <> show args.next <> ")"
  show (SupplyDimensions dims next) =
    "SupplyDimensions " <> show dims <> " (" <> show next <> ")"
  show (SupplyDisplayBounds bounds next) =
    "SupplyDisplayBounds " <> show bounds <> " (" <> show next <> ")"
  show (CalculatedDimensions bounds dims _ next) =
    "CalculatedDimensions " <> show bounds <> " " <> show dims <> " (" <> show next <> ")"

initAutomaton
  :: forall action slots m
   . RenderText
   -> AST action slots m
   -> DisplayBounds
   -> Automaton action slots m
initAutomaton renderText ast bounds =
  { stack: SupplyDisplayBounds bounds (Evaluate ast Bottom)
  , renderText
  }

mapAutomaton
  :: forall slots m action
   . (RenderText -> Frame action slots m -> Frame action slots m)
  -> Automaton action slots m
  -> Automaton action slots m
mapAutomaton f { stack, renderText } =
   { stack: _, renderText } $ f renderText stack

step1 :: forall action slots m. Automaton action slots m -> Automaton action slots m
step1 = mapAutomaton step1F

step1F
  :: forall action slots m
   . RenderText
  -> Frame action slots m
  -> Frame action slots m
step1F renderText = case _ of
  SupplyDimensions dims (EvaluateChild bounds slot _ next) ->
    CalculatedDimensions bounds dims (widget slot) next
  CalculatedDimensions _ dims vdom (EvaluateSubtrees args) ->
    EvaluateSubtrees $ args
      { childDims = args.childDims { height = args.childDims.height + dims.height }
      , evalF = over (arg <<< prop (Proxy :: _ "children")) (Cons vdom) args.evalF
      }
  CalculatedDimensions bounds dims dom1 (EvaluateAbsolute args) ->
    let { dom: dom2 } = args.bodyF { renderText, bounds, dims }
    in CalculatedDimensions bounds dims (HSE.g_ [ dom1, dom2 ]) args.next
  EvaluateSubtrees { children: Nil, childDims, evalF, next } ->
    let { dom, dims, bounds } = evalF { children: Nil, childDims }
    in CalculatedDimensions bounds dims dom next
  EvaluateSubtrees { children: Cons ast asts, childBounds, childDims, evalF, next } ->
    SupplyDisplayBounds (childBounds { y = childBounds.y + childDims.height }) $
      Evaluate ast $
        EvaluateSubtrees { children: asts, childBounds, childDims, evalF, next }

  -- This case (for evaluating an arbitrary AST) is the heart and soul of this
  -- automaton, and the one that's responsible for doing the bulk of the
  -- calculations. It needs to figure out what type of tree it's dealing with
  -- (a leaf, a container, or a child component) and calculate where possible,
  -- emit appropriate frames otherwise.
  SupplyDisplayBounds bounds (Evaluate ast next) -> case ast of
    ChildComponent slot boundsF ->
      EvaluateChild bounds slot boundsF next
    Rect props children ->
      evalRect bounds props children next
    Text props fontInfo text ->
      evalText renderText bounds props fontInfo text next
    Image props ->
      evalImage bounds props next
    ManualSVG bodyF ->
      let { dims, dom } = bodyF { renderText, bounds }
      in CalculatedDimensions bounds dims dom next
    ManualSVGAbsolute bodyF ast' ->
      SupplyDisplayBounds bounds $
        Evaluate ast' $
          EvaluateAbsolute { bodyF, next }

  other -> other

getOutput :: forall action slots m. Automaton action slots m -> Output action slots m
getOutput = getOutputF <<< _.stack

getOutputF :: forall action slots m. Frame action slots m -> Output action slots m
getOutputF = case _ of
  CalculatedDimensions bounds dims vdom Bottom -> Done bounds dims vdom
  EvaluateChild bounds _slot boundsF _next -> NeedsInput bounds boundsF

  SupplyDimensions _dims (EvaluateChild _bounds _slot _ _next) -> Continue
  EvaluateSubtrees _args -> Continue
  CalculatedDimensions _bounds _dims _vdom (EvaluateSubtrees _args) ->
    Continue
  CalculatedDimensions _bounds _dims _vdom (EvaluateAbsolute _args) ->
    Continue
  SupplyDisplayBounds _bounds (Evaluate _ast _next) -> Continue

  _ -> Crash

supplyDimensions
  :: forall action slots m
   . Dimensions
  -> Automaton action slots m
  -> Automaton action slots m
supplyDimensions dims automaton = automaton
  { stack = supplyDimensionsF dims automaton.stack
  }

supplyDimensionsF
  :: forall action slots m
   . Dimensions
  -> Frame action slots m
  -> Frame action slots m
supplyDimensionsF = SupplyDimensions

evalText
  :: forall action slots m
   . RenderText
  -> DisplayBounds
  -> Array (ASTProp ASTtext SVGMLtext action)
  -> FontInfo
  -> String
  -> Frame action slots m
  -> Frame action slots m
evalText renderText bounds props fontInfo@(Tuple size font) text next = do
  let margin = normalizeBox bounds.width svgmlProps.margin
  let lineHeight = svgmlProps.lineHeight * toNumber size
  let lines = renderText { text, fontInfo, displayWidth: bounds.width - margin.left - margin.right, overflow: svgmlProps.overflow }
  let dims =
        { width: bounds.width
        , height: round (toNumber (Array.length lines) * lineHeight) + margin.top + margin.bottom
        }
  case Array.uncons lines of
    Nothing -> next  -- No need to render anything if no text given.
    Just { head: l, tail: ls } -> do
      let dom =
            HSE.text
                ([ HSP.fontFamily font
                 , HSP.fontSize $ show size <> "px"
                 , HSP.x $ toNumber $ bounds.x + margin.left
                 , HSP.y $ toNumber $ bounds.y + margin.top
                 , HSP.dominantBaseline "hanging"
                 ]
                 <> map (\f -> f svgmlProps) textConvertSVGMLProps
                 <> domProps)
              (HSE.tspan
                -- First line requires special treatment for line height
                ([ alignmentX svgmlProps.textAlign l (toNumber $ bounds.x + margin.left)
                 , HSP.dy $ ((svgmlProps.lineHeight - 1.0) / 2.0) * toNumber size
                 ]
                 <> alignmentSpacing svgmlProps.textAlign l)
                [ HH.text l.text ]
              `Array.cons`
              (ls <#> \line ->
                HSE.tspan
                  ([ alignmentX svgmlProps.textAlign line (toNumber $ bounds.x + margin.left)
                   , HSP.dy lineHeight
                   ]
                   <> alignmentSpacing svgmlProps.textAlign l)
                  [ HH.text line.text ]))
      CalculatedDimensions bounds dims dom next
  where
    domProps :: Array (IProp SVGtext action)
    domProps = map expand $ flip mapMaybe props case _ of
      FromDOM prop -> Just prop
      FromSVG _ -> Nothing

    svgmlProps :: Record SVGMLtext
    svgmlProps =
      let SparseRecord builder = fold $ flip mapMaybe props case _ of
            FromDOM _ -> Nothing
            FromSVG svg -> Just svg
      in Builder.build builder defaultSVGMLtext

    textConvertSVGMLProps :: forall action'. Array (Record SVGMLtext -> IProp SVGtext action')
    textConvertSVGMLProps =
      [ over arg _.color HSP.fill
      , over arg _.cursor HSP.cursor
      ]

    alignmentX :: forall action'. TextAlign -> Line -> Number -> IProp SVGtspan action'
    alignmentX align line baseX = HSP.x case align of
      Center         -> baseX + (line.slop / 2.0)
      Justify        -> baseX
      LeftTextAlign  -> baseX
      RightTextAlign -> baseX + line.slop

    -- | For @Justify@ alignment only, we need to set the @word-spacing@ attribute.
    alignmentSpacing :: forall action'. TextAlign -> Line -> Array (IProp SVGtspan action')
    alignmentSpacing align line = case align of
      Justify -> if line.numWords < 2
        then []
        else [HSP.wordSpacing $ show $ line.slop / toNumber (line.numWords - 1)]
      _       -> []

type Box =
  { left :: Int
  , right :: Int
  , top :: Int
  , bottom :: Int
  }

type BoxModel =
  { margin :: Box
  , borderWidth :: Int
  , padding :: Box
  }

normalizeBox :: Int -> BoxParams -> Box
normalizeBox vec params =
  { left: normalizeUnit params.left vec
  , right: normalizeUnit params.right vec
  , top: normalizeUnit params.top vec
  , bottom: normalizeUnit params.bottom vec
  }

rectBoxModel :: Int -> Record SVGMLrect -> BoxModel
rectBoxModel width svgmlProps =
  -- We're intentionally having top/bottom margin and padding key off the
  -- width, when specified using percentages. This is a bit of a hack, but
  -- avoids having to do anything complicated.
  { margin: normalizeBox width svgmlProps.margin
  , borderWidth: svgmlProps.border.width
  , padding: normalizeBox width svgmlProps.padding
  }

imageBoxModel :: Int -> Record SVGMLimage -> BoxModel
imageBoxModel width svgmlProps =
  -- We're intentionally having top/bottom margin and padding key off the
  -- width, when specified using percentages. This is a bit of a hack, but
  -- avoids having to do anything complicated.
  { margin: normalizeBox width svgmlProps.margin
  , borderWidth: 0
  , padding: { left: 0, right: 0, top: 0, bottom: 0 }
  }

evalRect
  :: forall action slots m
   . DisplayBounds
  -> Array (ASTProp ASTrect SVGMLrect action)
  -> Array (AST action slots m)
  -> Frame action slots m
  -> Frame action slots m
evalRect bounds props children next =
  EvaluateSubtrees
    { children: List.fromFoldable children
    , childBounds: { width: childWidth, x: childX, y: childY }
    , childDims: { width: childWidth, height: 0 }
    , evalF: rectEvalSubtreeF bounds props svgmlProps boxModel
    , next
    }
  where
    svgmlProps :: Record SVGMLrect
    svgmlProps =
      let SparseRecord builder = fold $ flip mapMaybe props case _ of
            FromDOM _ -> Nothing
            FromSVG svg -> Just svg
      in Builder.build builder defaultSVGMLrect

    boxModel :: BoxModel
    boxModel = rectBoxModel bounds.width svgmlProps

    childWidth :: Int
    childWidth = bounds.width
      - boxModel.margin.left
      - boxModel.margin.right
      - (2 * boxModel.borderWidth)
      - boxModel.padding.left
      - boxModel.padding.right

    childX :: Int
    childX = bounds.x
      + boxModel.margin.left
      + boxModel.borderWidth
      + boxModel.padding.left

    childY :: Int
    childY = bounds.y
      + boxModel.margin.top
      + boxModel.borderWidth
      + boxModel.padding.top

rectEvalSubtreeF
  :: forall action slots m
   . DisplayBounds
  -> Array (ASTProp ASTrect SVGMLrect action)
  -> Record SVGMLrect
  -> BoxModel
  -> EvalSubtreeF action slots m
rectEvalSubtreeF bounds props svgmlProps boxModel = \{ children, childDims } ->
  { dom: HSE.g domProps $
      HSE.rect'
        ([ HSP.x (toNumber $ bounds.x + boxModel.margin.left + (boxModel.borderWidth / 2))
         , HSP.y (toNumber $ bounds.y + boxModel.margin.top + (boxModel.borderWidth / 2))
         , HSP.width (toNumber $ childDims.width + boxModel.padding.left + boxModel.padding.right + boxModel.borderWidth)
         , HSP.height (toNumber $ childDims.height + boxModel.padding.top + boxModel.padding.bottom + boxModel.borderWidth)
         ]
        <> map (\f -> f svgmlProps) rectConvertSVGMLProps)
      `Array.cons` (Array.fromFoldable children)
  , dims:
    { width: childDims.width
        + boxModel.margin.left
        + boxModel.margin.right
        + (2 * boxModel.borderWidth)
        + boxModel.padding.left
        + boxModel.padding.right
    , height: childDims.height
        + boxModel.margin.top
        + boxModel.margin.bottom
        + (2 * boxModel.borderWidth)
        + boxModel.padding.top
        + boxModel.padding.bottom
    }
  , bounds
  }
  where
    domProps :: Array (IProp SVGg action)
    domProps = map expand $ flip mapMaybe props case _ of
      FromDOM prop -> Just prop
      FromSVG _ -> Nothing

    rectConvertSVGMLProps :: forall action'. Array (Record SVGMLrect -> IProp SVGrect action')
    rectConvertSVGMLProps =
      [ over arg _.border.color HSP.stroke
      , over arg _.border.width (HSP.strokeWidth <<< toNumber)
      , over arg _.border.opacity HSP.strokeOpacity
      , over arg _.background.color HSP.fill
      , over arg _.background.opacity HSP.fillOpacity
      , over arg _.borderRadius.x (HSP.rx <<< toNumber)
      , over arg _.borderRadius.y (HSP.ry <<< toNumber)
      , over arg _.cursor HSP.cursor
      ]

evalImage
  :: forall action slots m
   . DisplayBounds
  -> Array (ASTProp ASTimage SVGMLimage action)
  -> Frame action slots m
  -> Frame action slots m
evalImage bounds props next =
  CalculatedDimensions
    bounds
    { width: innerWidth
        + boxModel.margin.left
        + boxModel.margin.right
        + (2 * boxModel.borderWidth)
        + boxModel.padding.left
        + boxModel.padding.right
    , height: innerHeight
        + boxModel.margin.top
        + boxModel.margin.bottom
        + (2 * boxModel.borderWidth)
        + boxModel.padding.top
        + boxModel.padding.bottom
    }
    (HSE.g domProps $ Array.singleton $
      HSE.image'
        ([ HSP.x (toNumber $ bounds.x + boxModel.margin.left)
         , HSP.y (toNumber $ bounds.y + boxModel.margin.top)
         , HSP.width $ toNumber innerWidth
         , HSP.height $ toNumber innerHeight
         , HSP.preserveAspectRatio "none"
         ]
        <> map (\f -> f svgmlProps) imageConvertSVGMLProps))
    next
  where
    svgmlProps :: Record SVGMLimage
    svgmlProps =
      let SparseRecord builder = fold $ flip mapMaybe props case _ of
            FromDOM _ -> Nothing
            FromSVG svg -> Just svg
      in Builder.build builder defaultSVGMLimage

    boxModel = imageBoxModel bounds.width svgmlProps

    innerWidth :: Int
    innerWidth = bounds.width
      - boxModel.margin.left
      - boxModel.margin.right
      - (2 * boxModel.borderWidth)
      - boxModel.padding.left
      - boxModel.padding.right

    innerHeight :: Int
    innerHeight = round $ toNumber innerWidth * svgmlProps.yRatio

    domProps :: Array (IProp SVGg action)
    domProps = map expand $ flip mapMaybe props case _ of
      FromDOM prop -> Just prop
      FromSVG _ -> Nothing

    imageConvertSVGMLProps :: forall action'. Array (Record SVGMLimage -> IProp SVGimage action')
    imageConvertSVGMLProps =
      [ over arg _.cursor HSP.cursor
      , over arg _.opacity HSP.opacity
      , over arg _.imageRendering (HSP.imageRendering <<< case _ of
          ImageRenderingAuto -> "auto"
          ImageRenderingOptimizeSpeed -> "optimizeSpeed"
          ImageRenderingOptimizeQuality -> "optimizeQuality")
      , over arg _.href HSP.href
      ]

-- This is slightly tricky from a library perspective. The information we need
-- is the dimensions of the image. But we can't possible know that from the
-- information we've been given! If we wanted that, we'd have to pull down
-- the image itself and check, which is clearly not something we can do.
-- Theoretically, we could do something like set up an event handler and
-- wait for the image to load. But that seems kind of dumb, to be honest.
-- We should probably not do that. Instead, we probably have to force the
-- user to provide height information... which feels pretty bad.
