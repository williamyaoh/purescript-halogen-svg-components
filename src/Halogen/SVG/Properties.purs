module Halogen.SVG.Properties
  ( module Halogen.HTML.Properties
  , module Halogen.HTML.Events
  , attr
  , id
  , class_
  , classes
  , requiredExtensions
  , systemLanguage
  , xmlLang
  , alignmentBaseline
  , baselineShift
  , clip
  , clipPath
  , clipRule
  , color
  , colorInterpolation
  , colorInterpolationFilters
  , colorProfile
  , colorRendering
  , cursor
  , direction
  , display
  , dominantBaseline
  , enableBackground
  , fill
  , fillOpacity
  , fillRule
  , filter
  , floodColor
  , floodOpacity
  , fontFamily
  , fontSize
  , fontSizeAdjust
  , fontStretch
  , fontStyle
  , fontVariant
  , fontWeight
  , imageRendering
  , letterSpacing
  , lightingColor
  , markerEnd
  , markerMid
  , markerStart
  , mask
  , opacity
  , overflow
  , pointerEvents
  , shapeRendering
  , stopColor
  , stopOpacity
  , stroke
  , strokeDashArray
  , strokeDashOffset
  , strokeLineCap
  , strokeLineJoin
  , strokeMiterLimit
  , strokeOpacity
  , strokeWidth
  , textAnchor
  , textDecoration
  , textRendering
  , unicodeBidi
  , visibility
  , wordSpacing
  , writingMode
  , attributeName
  , begin
  , dur
  , end
  , min
  , max
  , restart
  , repeatCount
  , repeatDur
  , calcMode
  , values
  , keyTimes
  , keySplines
  , from
  , to
  , by
  , additive
  , accumulate
  , x
  , y
  , z
  , width
  , height
  , result
  , tableValues
  , intercept
  , amplitude
  , exponent
  , offset
  , transform
  , href
  , xlinkHref
  , path
  , keyPoints
  , rotate
  , cx
  , cy
  , r
  , clipPathUnits
  , rx
  , ry
  , in_
  , in2
  , mode
  , operator
  , k1
  , k2
  , k3
  , k4
  , order
  , kernelMatrix
  , divisor
  , bias
  , targetX
  , targetY
  , edgeMode
  , preserveAlpha
  , surfaceScale
  , diffuseConstant
  , scale
  , xChannelSelector
  , yChannelSelector
  , azimuth
  , elevation
  , stdDeviation
  , preserveAspectRatio
  , radius
  , dx
  , dy
  , specularConstant
  , specularExponent
  , pointsAtX
  , pointsAtY
  , pointsAtZ
  , limitingConeAngle
  , baseFrequency
  , numOctaves
  , seed
  , stitchTiles
  , filterRes
  , filterUnits
  , primitiveUnits
  , x1
  , y1
  , x2
  , y2
  , gradientUnits
  , gradientTransform
  , spreadMethod
  , viewBox
  , refX
  , refY
  , markerUnits
  , markerWidth
  , markerHeight
  , orient
  , maskUnits
  , maskContentUnits
  , d
  , pathLength
  , patternUnits
  , patternContentUnits
  , patternTransform
  , points
  , fx
  , fy
  , media
  , lengthAdjust
  , textLength
  , startOffset
  , method
  , spacing
  , onZoom
  , onUnload
  , onActivate
  , onBegin
  , onEnd
  , onRepeat
  )
where

import Prelude

import Data.String ( joinWith )
import Data.Newtype ( unwrap )

import Web.Event.Event ( Event, EventType(..) )
import Web.HTML.Event.EventTypes as ET

import Halogen.HTML.Core ( AttrName(..), ClassName )
import Halogen.HTML.Properties
  ( style
  , target
  , title
  , type_
  )
import Halogen.HTML.Properties as HP
import Halogen.HTML.Events
  ( onAbort
  , onError
  , onScroll
  , onLoad
  , onFocusIn , onFocusOut
  , onClick
  , onMouseDown, onMouseUp
  , onMouseOver, onMouseOut
  , onMouseMove
  )
import Halogen.HTML.Events as HE

-- Weird trick to avoid exporting this type from the module along with
-- Halogen.HTML.Properties
type IProp r i = HP.IProp r i

attr :: forall r i. AttrName -> String -> IProp r i
attr = HP.attr

id :: forall r i. String -> IProp ("id" :: String | r) i
id = attr (AttrName "id")

class_ :: forall r i. ClassName -> IProp ("class" :: String | r) i
class_ = attr (AttrName "class") <<< unwrap

classes :: forall r i. Array ClassName -> IProp ("class" :: String | r) i
classes = attr (AttrName "class") <<< joinWith " " <<< map unwrap

requiredExtensions :: forall r i. String -> IProp ("requiredExtensions" :: String | r) i
requiredExtensions = attr (AttrName "requiredExtensions")

systemLanguage :: forall r i. String -> IProp ("systemLanguage" :: String | r) i
systemLanguage = attr (AttrName "systemLanguage")

xmlLang :: forall r i. String -> IProp ("xml:lang" :: String | r) i
xmlLang = attr (AttrName "xml:lang")

alignmentBaseline :: forall r i. String -> IProp ("alignmentBaseline" :: String | r) i
alignmentBaseline = attr (AttrName "alignment-baseline")

baselineShift :: forall r i. String -> IProp ("baselineShift" :: String | r) i
baselineShift = attr (AttrName "baseline-shift")

clip :: forall r i. String -> IProp ("clip" :: String | r) i
clip = attr (AttrName "clip")

clipPath :: forall r i. String -> IProp ("clipPath" :: String | r) i
clipPath = attr (AttrName "clip-path")

clipRule :: forall r i. String -> IProp ("clipRule" :: String | r) i
clipRule = attr (AttrName "clip-rule")

color :: forall r i. String -> IProp ("color" :: String | r) i
color = attr (AttrName "color")

colorInterpolation :: forall r i. String -> IProp ("colorInterpolation" :: String | r) i
colorInterpolation = attr (AttrName "color-interpolation")

colorInterpolationFilters :: forall r i. String -> IProp ("colorInterpolationFilters" :: String | r) i
colorInterpolationFilters = attr (AttrName "color-interpolation-filters")

colorProfile :: forall r i. String -> IProp ("colorProfile" :: String | r) i
colorProfile = attr (AttrName "color-profile")

colorRendering :: forall r i. String -> IProp ("colorRendering" :: String | r) i
colorRendering = attr (AttrName "color-rendering")

cursor :: forall r i. String -> IProp ("cursor" :: String | r) i
cursor = attr (AttrName "cursor")

direction :: forall r i. String -> IProp ("direction" :: String | r) i
direction = attr (AttrName "direction")

display :: forall r i. String -> IProp ("display" :: String | r) i
display = attr (AttrName "display")

dominantBaseline :: forall r i. String -> IProp ("dominantBaseline" :: String | r) i
dominantBaseline = attr (AttrName "dominant-baseline")

enableBackground :: forall r i. String -> IProp ("enableBackground" :: String | r) i
enableBackground = attr (AttrName "enable-background")

fill :: forall r i. String -> IProp ("fill" :: String | r) i
fill = attr (AttrName "fill")

fillOpacity :: forall r i. Number -> IProp ("fillOpacity" :: Number | r) i
fillOpacity = attr (AttrName "fill-opacity") <<< show

fillRule :: forall r i. String -> IProp ("fillRule" :: String | r) i
fillRule = attr (AttrName "fill-rule")

filter :: forall r i. String -> IProp ("filter" :: String | r) i
filter = attr (AttrName "filter")

floodColor :: forall r i. String -> IProp ("floodColor" :: String | r) i
floodColor = attr (AttrName "flood-color")

floodOpacity :: forall r i. Number -> IProp ("floodOpacity" :: Number | r) i
floodOpacity = attr (AttrName "flood-opacity") <<< show

fontFamily :: forall r i. String -> IProp ("fontFamily" :: String | r) i
fontFamily = attr (AttrName "font-family")

fontSize :: forall r i. String -> IProp ("fontSize" :: String | r) i
fontSize = attr (AttrName "font-size")

fontSizeAdjust :: forall r i. Number -> IProp ("fontSizeAdjust" :: Number | r) i
fontSizeAdjust = attr (AttrName "font-size-adjust") <<< show

fontStretch :: forall r i. String -> IProp ("fontStretch" :: String | r) i
fontStretch = attr (AttrName "font-stretch")

fontStyle :: forall r i. String -> IProp ("fontStyle" :: String | r) i
fontStyle = attr (AttrName "font-style")

fontVariant :: forall r i. String -> IProp ("fontVariant" :: String | r) i
fontVariant = attr (AttrName "font-variant")

fontWeight :: forall r i. String -> IProp ("fontWeight" :: String | r) i
fontWeight = attr (AttrName "font-weight")

imageRendering :: forall r i. String -> IProp ("imageRendering" :: String | r) i
imageRendering = attr (AttrName "image-rendering")

letterSpacing :: forall r i. String -> IProp ("letterSpacing" :: String | r) i
letterSpacing = attr (AttrName "letter-spacing")

lightingColor :: forall r i. String -> IProp ("lightingColor" :: String | r) i
lightingColor = attr (AttrName "lighting-color")

markerEnd :: forall r i. String -> IProp ("markerEnd" :: String | r) i
markerEnd = attr (AttrName "marker-end")

markerMid :: forall r i. String -> IProp ("markerMid" :: String | r) i
markerMid = attr (AttrName "marker-mid")

markerStart :: forall r i. String -> IProp ("markerStart" :: String | r) i
markerStart = attr (AttrName "marker-start")

mask :: forall r i. String -> IProp ("mask" :: String | r) i
mask = attr (AttrName "mask")

opacity :: forall r i. Number -> IProp ("opacity" :: Number | r) i
opacity = attr (AttrName "opacity") <<< show

overflow :: forall r i. String -> IProp ("overflow" :: String | r) i
overflow = attr (AttrName "overflow")

pointerEvents :: forall r i. String -> IProp ("pointerEvents" :: String | r) i
pointerEvents = attr (AttrName "pointer-events")

shapeRendering :: forall r i. String -> IProp ("shapeRendering" :: String | r) i
shapeRendering = attr (AttrName "shape-rendering")

stopColor :: forall r i. String -> IProp ("stopColor" :: String | r) i
stopColor = attr (AttrName "stop-color")

stopOpacity :: forall r i. Number -> IProp ("stopOpacity" :: Number | r) i
stopOpacity = attr (AttrName "stop-opacity") <<< show

stroke :: forall r i. String -> IProp ("stroke" :: String | r) i
stroke = attr (AttrName "stroke")

strokeDashArray :: forall r i. String -> IProp ("strokeDashArray" :: String | r) i
strokeDashArray = attr (AttrName "stroke-dasharray")

strokeDashOffset :: forall r i. String -> IProp ("strokeDashOffset" :: String | r) i
strokeDashOffset = attr (AttrName "stroke-dashoffset")

strokeLineCap :: forall r i. String -> IProp ("strokeLineCap" :: String | r) i
strokeLineCap = attr (AttrName "stroke-linecap")

strokeLineJoin :: forall r i. String -> IProp ("strokeLineJoin" :: String | r) i
strokeLineJoin = attr (AttrName "stroke-linejoin")

strokeMiterLimit :: forall r i. Number -> IProp ("strokeMiterLimit" :: Number | r) i
strokeMiterLimit = attr (AttrName "stroke-miterlimit") <<< show

strokeOpacity :: forall r i. Number -> IProp ("strokeOpacity" :: Number | r) i
strokeOpacity = attr (AttrName "stroke-opacity") <<< show

strokeWidth :: forall r i. Number -> IProp ("strokeWidth" :: Number | r) i
strokeWidth = attr (AttrName "stroke-width") <<< show

textAnchor :: forall r i. String -> IProp ("textAnchor" :: String | r) i
textAnchor = attr (AttrName "text-anchor")

textDecoration :: forall r i. String -> IProp ("textDecoration" :: String | r) i
textDecoration = attr (AttrName "text-decoration")

textRendering :: forall r i. String -> IProp ("textRendering" :: String | r) i
textRendering = attr (AttrName "text-rendering")

unicodeBidi :: forall r i. String -> IProp ("unicodeBidi" :: String | r) i
unicodeBidi = attr (AttrName "unicode-bidi")

visibility :: forall r i. String -> IProp ("visibility" :: String | r) i
visibility = attr (AttrName "visibility")

wordSpacing :: forall r i. String -> IProp ("wordSpacing" :: String | r) i
wordSpacing = attr (AttrName "word-spacing")

writingMode :: forall r i. String -> IProp ("writingMode" :: String | r) i
writingMode = attr (AttrName "writing-mode")

attributeName :: forall r i. String -> IProp ("attributeName" :: String | r) i
attributeName = attr (AttrName "attributeName")

begin :: forall r i. String -> IProp ("begin" :: String | r) i
begin = attr (AttrName "begin")

dur :: forall r i. String -> IProp ("dur" :: String | r) i
dur = attr (AttrName "dur")

end :: forall r i. String -> IProp ("end" :: String | r) i
end = attr (AttrName "end")

min :: forall r i. String -> IProp ("min" :: String | r) i
min = attr (AttrName "min")

max :: forall r i. String -> IProp ("max" :: String | r) i
max = attr (AttrName "max")

restart :: forall r i. String -> IProp ("restart" :: String | r) i
restart = attr (AttrName "restart")

repeatCount :: forall r i. String -> IProp ("repeatCount" :: String | r) i
repeatCount = attr (AttrName "repeatCount")

repeatDur :: forall r i. String -> IProp ("repeatDur" :: String | r) i
repeatDur = attr (AttrName "repeatDur")

calcMode :: forall r i. String -> IProp ("calcMode" :: String | r) i
calcMode = attr (AttrName "calcMode")

values :: forall r i. String -> IProp ("values" :: String | r) i
values = attr (AttrName "values")

keyTimes :: forall r i. String -> IProp ("keyTimes" :: String | r) i
keyTimes = attr (AttrName "keyTimes")

keySplines :: forall r i. String -> IProp ("keySplines" :: String | r) i
keySplines = attr (AttrName "keySplines")

from :: forall r i. String -> IProp ("from" :: String | r) i
from = attr (AttrName "from")

to :: forall r i. String -> IProp ("to" :: String | r) i
to = attr (AttrName "to")

by :: forall r i. String -> IProp ("by" :: String | r) i
by = attr (AttrName "by")

additive :: forall r i. String -> IProp ("additive" :: String | r) i
additive = attr (AttrName "additive")

accumulate :: forall r i. String -> IProp ("accumulate" :: String | r) i
accumulate = attr (AttrName "accumulate")

x :: forall r i. Number -> IProp ("x" :: Number | r) i
x = attr (AttrName "x") <<< show

y :: forall r i. Number -> IProp ("y" :: Number | r) i
y = attr (AttrName "y") <<< show

z :: forall r i. String -> IProp ("z" :: String | r) i
z = attr (AttrName "z") <<< show

width :: forall r i. Number -> IProp ("width" :: Number | r) i
width = attr (AttrName "width") <<< show

height :: forall r i. Number -> IProp ("height" :: Number | r) i
height = attr (AttrName "height") <<< show

result :: forall r i. String -> IProp ("result" :: String | r) i
result = attr (AttrName "result")

tableValues :: forall r i. String -> IProp ("tableValues" :: String | r) i
tableValues = attr (AttrName "tableValues")

intercept :: forall r i. Number -> IProp ("intercept" :: Number | r) i
intercept = attr (AttrName "intercept") <<< show

amplitude :: forall r i. Number -> IProp ("amplitude" :: Number | r) i
amplitude = attr (AttrName "amplitude") <<< show

exponent :: forall r i. Number -> IProp ("exponent" :: Number | r) i
exponent = attr (AttrName "exponent") <<< show

offset :: forall r i. String -> IProp ("offset" :: String | r) i
offset = attr (AttrName "offset")

transform :: forall r i. String -> IProp ("transform" :: String | r) i
transform = attr (AttrName "transform")

href :: forall r i. String -> IProp ("href" :: String | r) i
href = attr (AttrName "href")

xlinkHref :: forall r i. String -> IProp ("xlink:href" :: String | r) i
xlinkHref = attr (AttrName "xlink:href")

path :: forall r i. String -> IProp ("path" :: String | r) i
path = attr (AttrName "path")

keyPoints :: forall r i. String -> IProp ("keyPoints" :: String | r) i
keyPoints = attr (AttrName "keyPoints")

rotate :: forall r i. String -> IProp ("rotate" :: String | r) i
rotate = attr (AttrName "rotate")

cx :: forall r i. Number -> IProp ("cx" :: Number | r) i
cx = attr (AttrName "cx") <<< show

cy :: forall r i. Number -> IProp ("cy" :: Number | r) i
cy = attr (AttrName "cy") <<< show

r :: forall r i. Number -> IProp ("r" :: Number | r) i
r = attr (AttrName "r") <<< show

clipPathUnits :: forall r i. String -> IProp ("clipPathUnits" :: String | r) i
clipPathUnits = attr (AttrName "clipPathUnits")

rx :: forall r i. Number -> IProp ("rx" :: Number | r) i
rx = attr (AttrName "rx") <<< show

ry :: forall r i. Number -> IProp ("ry" :: Number | r) i
ry = attr (AttrName "ry") <<< show

in_ :: forall r i. String -> IProp ("in" :: String | r) i
in_ = attr (AttrName "in")

in2 :: forall r i. String -> IProp ("in2" :: String | r) i
in2 = attr (AttrName "in2")

mode :: forall r i. String -> IProp ("mode" :: String | r) i
mode = attr (AttrName "mode")

operator :: forall r i. String -> IProp ("operator" :: String | r) i
operator = attr (AttrName "operator")

k1 :: forall r i. Number -> IProp ("k1" :: Number | r) i
k1 = attr (AttrName "k1") <<< show

k2 :: forall r i. Number -> IProp ("k2" :: Number | r) i
k2 = attr (AttrName "k2") <<< show

k3 :: forall r i. Number -> IProp ("k3" :: Number | r) i
k3 = attr (AttrName "k3") <<< show

k4 :: forall r i. Number -> IProp ("k4" :: Number | r) i
k4 = attr (AttrName "k4") <<< show

order :: forall r i. String -> IProp ("order" :: String | r) i
order = attr (AttrName "order")

kernelMatrix :: forall r i. String -> IProp ("kernelMatrix" :: String | r) i
kernelMatrix = attr (AttrName "kernelMatrix")

divisor :: forall r i. Number -> IProp ("divisor" :: Number | r) i
divisor = attr (AttrName "divisor") <<< show

bias :: forall r i. Number -> IProp ("bias" :: Number | r) i
bias = attr (AttrName "bias") <<< show

targetX :: forall r i. Int -> IProp ("targetX" :: Int | r) i
targetX = attr (AttrName "targetX") <<< show

targetY :: forall r i. Int -> IProp ("targetY" :: Int | r) i
targetY = attr (AttrName "targetY") <<< show

edgeMode :: forall r i. String -> IProp ("edgeMode" :: String | r) i
edgeMode = attr (AttrName "edgeMode")

preserveAlpha :: forall r i. Boolean -> IProp ("preserveAlpha" :: Boolean | r) i
preserveAlpha = attr (AttrName "preserveAlpha") <<< case _ of
  true -> "true"
  false -> "false"

surfaceScale :: forall r i. Number -> IProp ("surfaceScale" :: Number | r) i
surfaceScale = attr (AttrName "surfaceScale") <<< show

diffuseConstant :: forall r i. Number -> IProp ("diffuseConstant" :: Number | r) i
diffuseConstant = attr (AttrName "diffuseConstant") <<< show

scale :: forall r i. Number -> IProp ("scale" :: Number | r) i
scale = attr (AttrName "scale") <<< show

xChannelSelector :: forall r i. String -> IProp ("xChannelSelector" :: String | r) i
xChannelSelector = attr (AttrName "xChannelSelector")

yChannelSelector :: forall r i. String -> IProp ("yChannelSelector" :: String | r) i
yChannelSelector = attr (AttrName "yChannelSelector")

azimuth :: forall r i. Number -> IProp ("azimuth" :: Number | r) i
azimuth = attr (AttrName "azimuth") <<< show

elevation :: forall r i. Number -> IProp ("elevation" :: Number | r) i
elevation = attr (AttrName "elevation") <<< show

stdDeviation :: forall r i. String -> IProp ("stdDeviation" :: String | r) i
stdDeviation = attr (AttrName "stdDeviation")

preserveAspectRatio :: forall r i. String -> IProp ("preserveAspectRatio" :: String | r) i
preserveAspectRatio = attr (AttrName "preserveAspectRatio")

radius :: forall r i. String -> IProp ("radius" :: String | r) i
radius = attr (AttrName "radius")

dx :: forall r i. Number -> IProp ("dx" :: Number | r) i
dx = attr (AttrName "dx") <<< show

dy :: forall r i. Number -> IProp ("dy" :: Number | r) i
dy = attr (AttrName "dy") <<< show

specularConstant :: forall r i. Number -> IProp ("specularConstant" :: Number | r) i
specularConstant = attr (AttrName "specularConstant") <<< show

specularExponent :: forall r i. Number -> IProp ("specularExponent" :: Number | r) i
specularExponent = attr (AttrName "specularExponent") <<< show

pointsAtX :: forall r i. Number -> IProp ("pointsAtX" :: Number | r) i
pointsAtX = attr (AttrName "pointsAtX") <<< show

pointsAtY :: forall r i. Number -> IProp ("pointsAtY" :: Number | r) i
pointsAtY = attr (AttrName "pointsAtY") <<< show

pointsAtZ :: forall r i. Number -> IProp ("pointsAtZ" :: Number | r) i
pointsAtZ = attr (AttrName "pointsAtZ") <<< show

limitingConeAngle :: forall r i. Number -> IProp ("limitingConeAngle" :: Number | r) i
limitingConeAngle = attr (AttrName "limitingConeAngle") <<< show

baseFrequency :: forall r i. String -> IProp ("baseFrequency" :: String | r) i
baseFrequency = attr (AttrName "baseFrequency")

numOctaves :: forall r i. Int -> IProp ("numOctaves" :: Int | r) i
numOctaves = attr (AttrName "numOctaves") <<< show

seed :: forall r i. Number -> IProp ("seed" :: Number | r) i
seed = attr (AttrName "seed") <<< show

stitchTiles :: forall r i. String -> IProp ("stitchTiles" :: String | r) i
stitchTiles = attr (AttrName "stitchTiles")

filterRes :: forall r i. String -> IProp ("filterRes" :: String | r) i
filterRes = attr (AttrName "filterRes")

filterUnits :: forall r i. String -> IProp ("filterUnits" :: String | r) i
filterUnits = attr (AttrName "filterUnits")

primitiveUnits :: forall r i. String -> IProp ("primitiveUnits" :: String | r) i
primitiveUnits = attr (AttrName "primitiveUnits")

x1 :: forall r i. Number -> IProp ("x1" :: Number | r) i
x1 = attr (AttrName "x1") <<< show

y1 :: forall r i. Number -> IProp ("y1" :: Number | r) i
y1 = attr (AttrName "y1") <<< show

x2 :: forall r i. Number -> IProp ("x2" :: Number | r) i
x2 = attr (AttrName "x2") <<< show

y2 :: forall r i. Number -> IProp ("y2" :: Number | r) i
y2 = attr (AttrName "y2") <<< show

gradientUnits :: forall r i. String -> IProp ("gradientUnits" :: String | r) i
gradientUnits = attr (AttrName "gradientUnits")

gradientTransform :: forall r i. String -> IProp ("gradientTransform" :: String | r) i
gradientTransform = attr (AttrName "gradientTransform")

spreadMethod :: forall r i. String -> IProp ("spreadMethod" :: String | r) i
spreadMethod = attr (AttrName "spreadMethod")

viewBox :: forall r i. Number -> Number -> Number -> Number -> IProp ("viewBox" :: String | r) i
viewBox x' y' w h =
  let viewBoxStr = joinWith " " $ map show [ x', y', w, h ]
  in attr (AttrName "viewBox") viewBoxStr

refX :: forall r i. Number -> IProp ("refX" :: Number | r) i
refX = attr (AttrName "refX") <<< show

refY :: forall r i. Number -> IProp ("refY" :: Number | r) i
refY = attr (AttrName "refY") <<< show

markerUnits :: forall r i. String -> IProp ("markerUnits" :: String | r) i
markerUnits = attr (AttrName "markerUnits")

markerWidth :: forall r i. Number -> IProp ("markerWidth" :: Number | r) i
markerWidth = attr (AttrName "markerWidth") <<< show

markerHeight :: forall r i. Number -> IProp ("markerHeight" :: Number | r) i
markerHeight = attr (AttrName "markerHeight") <<< show

orient :: forall r i. String -> IProp ("orient" :: String | r) i
orient = attr (AttrName "orient")

maskUnits :: forall r i. String -> IProp ("maskUnits" :: String | r) i
maskUnits = attr (AttrName "maskUnits")

maskContentUnits :: forall r i. String -> IProp ("maskContentUnits" :: String | r) i
maskContentUnits = attr (AttrName "maskContentUnits")

d :: forall r i. String -> IProp ("d" :: String | r) i
d = attr (AttrName "d")

pathLength :: forall r i. Number -> IProp ("pathLength" :: Number | r) i
pathLength = attr (AttrName "pathLength") <<< show

patternUnits :: forall r i. String -> IProp ("patternUnits" :: String | r) i
patternUnits = attr (AttrName "patternUnits")

patternContentUnits :: forall r i. String -> IProp ("patternContentUnits" :: String | r) i
patternContentUnits = attr (AttrName "patternContentUnits")

patternTransform :: forall r i. String -> IProp ("patternTransform" :: String | r) i
patternTransform = attr (AttrName "patternTransform")

points :: forall r i. String -> IProp ("points" :: String | r) i
points = attr (AttrName "points")

fx :: forall r i. Number -> IProp ("fx" :: Number | r) i
fx = attr (AttrName "fx") <<< show

fy :: forall r i. Number -> IProp ("fy" :: Number | r) i
fy = attr (AttrName "fy") <<< show

media :: forall r i. String -> IProp ("media" :: String | r) i
media = attr (AttrName "media")

lengthAdjust :: forall r i. String -> IProp ("lengthAdjust" :: String | r) i
lengthAdjust = attr (AttrName "lengthAdjust")

textLength :: forall r i. String -> IProp ("textLength" :: String | r) i
textLength = attr (AttrName "textLength")

startOffset :: forall r i. Number -> IProp ("startOffset" :: Number | r) i
startOffset = attr (AttrName "startOffset") <<< show

method :: forall r i. String -> IProp ("method" :: String | r) i
method = attr (AttrName "method")

spacing :: forall r i. String -> IProp ("spacing" :: String | r) i
spacing = attr (AttrName "spacing")

--------------------------------------------------------------------------------

onZoom :: forall r i. (Event -> i) -> IProp ("onZoom" :: Event | r) i
onZoom = HE.handler zoomTy

onUnload :: forall r i. (Event -> i) -> IProp ("onUnload" :: Event | r) i
onUnload = HE.handler ET.unload

onActivate :: forall r i. (Event -> i) -> IProp ("onActivate" :: Event | r) i
onActivate = HE.handler activateTy

onBegin :: forall r i. (Event -> i) -> IProp ("onBegin" :: Event | r) i
onBegin = HE.handler beginTy

onEnd :: forall r i. (Event -> i) -> IProp ("onEnd" :: Event | r) i
onEnd = HE.handler endTy

onRepeat :: forall r i. (Event -> i) -> IProp ("onRepeat" :: Event | r) i
onRepeat = HE.handler repeatTy

--------------------------------------------------------------------------------

zoomTy :: EventType
zoomTy = EventType "zoom"

activateTy :: EventType
activateTy = EventType "activate"

beginTy :: EventType
beginTy = EventType "begin"

endTy :: EventType
endTy = EventType "end"

repeatTy :: EventType
repeatTy = EventType "repeat"
