module Data.Lens.Setter.Argument
  ( arg )
where

import Prelude

import Data.Lens ( Setter )

-- | A `Setter' to transform over the argument of a function.
-- | Useful for working with continuation-passing style.
arg :: forall a b r. Setter (b -> r) (a -> r) a b
arg f k = f >>> k
