"use strict";

// We only create one canvas element to avoid tanking performance by constantly
// creating and garbage collecting DOM nodes.
//
// Why is this defined in such a convoluted way? For instance, a much easier
// way to define this would be
//
// > var canvas = document.createElement("canvas");
//
// Defining it that way, however, means that we call .createElement() as soon
// as the compiled bundle gets loaded. That causes a crash in non-browser
// contexts like Node or PSCI. Which is bad.
var canvas = (function() {
    var c = null;
    return function() {
        if (c) {
            return c;
        } else {
            return c = document.createElement("canvas");
        }
    };
})();

export const pixelWidth_ = (s, font) => {
    const context = canvas().getContext("2d", {
        alpha: false,
        desynchronized: true
    });

    context.font = font;
    const metrics = context.measureText(s);

    return metrics.width;
};
