-- | Rendering functions for figuring out how much space a given bit of
-- | text will occupy on-screen. Useful for layouting things in either an
-- | SVG or HTML Canvas context.
-- |
-- | Note that this code will only work when run in a browser context; don't
-- | attempt to call it from Node.

module Data.String.Render
  ( Line
  , Overflow(..)
  , pixelWidth
  , breakLines
  )
where

import Prelude

import Control.Monad.State ( State, evalState, get, gets, modify_ )
import Data.Foldable ( intercalate )
import Data.Function.Uncurried ( Fn2, runFn2 )
import Data.Generic.Rep ( class Generic )
import Data.Hashable ( class Hashable, hash )
import Data.List ( List(..), (:), reverse )
import Data.Maybe ( Maybe(..) )
import Data.Show.Generic ( genericShow )
import Data.String.Utils ( words )

import Data.Array as Array
import Data.List as List
import Data.String as String

data Overflow
  = Visible
  | Hidden
  | Truncate

derive instance eqOverflow :: Eq Overflow
derive instance genericOverflow :: Generic Overflow _
instance showOverflow :: Show Overflow where
  show = genericShow
instance hashableOverflow :: Hashable Overflow where
  hash = hash <<< show

-- | Later parts of our SVG rendering pipeline can align text in various ways,
-- | like centering or doing justify alignment. Those parts need some extra
-- | information beyond just the line breaks, in this case the leftover line
-- | space and # of words.
type Line =
  { text :: String
  , slop :: Number  -- Leftover space.
  , numWords :: Int
  }

type RenderContext =
  { renderWord :: String -> Number
  , lineLen :: Number
  , overflow :: Overflow
  }

foreign import pixelWidth_ :: Fn2 String String Number

-- | @pixelWidth text font@ will give the width of said text
-- | when rendered with the specified font.
-- |
-- | An important caveat: this function is /very/ slow. It's highly recommended
-- | that you cache the result of calling this function in some way; if you
-- | call this on every frame, the underlying calls to @.measureText()@ will
-- | tank your performance.
pixelWidth :: String -> String -> Number
pixelWidth s font = runFn2 pixelWidth_ s font

-- | Break the given text into a sequence of lines that fit within the
-- | specified line length, in pixels.
-- |
-- | Since you're likely going to passing `pixelWidth' as your rendering
-- | function, the same caveat applies: cache the result of this function
-- | and don't call it too often, or you're going to tank your performance.
breakLines :: RenderContext -> String -> Array Line
breakLines ctx@{ renderWord, lineLen, overflow } s =
  let ws = List.fromFoldable $ Array.filter (not <<< String.null) (words s)
  in Array.fromFoldable (go ws)
  where
    go :: List String -> List Line
    go Nil = Nil
    go ws =
      let
        { line, rest } =
          evalState takeLine { remlen: lineLen, line: Nil, words: ws }
      in line : go rest

    takeLine :: State
      { remlen :: Number, line :: List String, words :: List String }
      { line :: Line, rest :: List String }
    takeLine = do
      words <- gets _.words
      case List.uncons words of
        Nothing -> do
          { remlen, line: l } <- get
          let line = { text: intercalate " " $ reverse l, slop: remlen, numWords: List.length l }
          pure { line, rest: Nil }
        Just { head, tail } -> do
          { remlen, line: l } <- get
          let
            wordlen =
              renderWord head
                + if List.null l then 0.0 else renderWord " "
          if wordlen <= remlen then do
            modify_ \s' -> s'
              { remlen = remlen - wordlen
              , line = head : l
              , words = tail
              }
            takeLine
          else if remlen == lineLen then do
            -- Do something special based on the context if the word is longer than a line.
            -- We only want to do this check if we have to, because it's expensive.
            case overflow of
              Visible -> modify_ \s' -> s'
                { remlen = 0.0
                , line = head : l
                , words = tail
                }
              Hidden -> do
                let { remlen, hidden } = calcHiddenLength ctx head
                modify_ \s' -> s'
                  { remlen = remlen
                  , line = hidden : l
                  , words = tail
                  }
              Truncate -> do
                let { remlen, truncate } = calcTruncateLength ctx head
                modify_ \s' -> s'
                  { remlen = remlen
                  , line = truncate : l
                  , words = tail
                  }
            takeLine
          else do
            let line = { text: intercalate " " $ reverse l, slop: remlen, numWords: List.length l }
            pure { line, rest: words }

-- Super wack looking function definition. Not sure if there's a good way
-- to improve this.

-- | Calculates the longest prefix of `word' that fits within `lineLen'.
-- | The easiest way to do this is a binary search on the rendered length,
-- | since it's monotonically increasing.
calcHiddenLength :: RenderContext -> String -> { remlen :: Number, hidden :: String }
calcHiddenLength { renderWord, lineLen } word =
  let
    charLen = String.length word
    { maxWord, maxLen } = go 0 charLen (charLen / 2) "" 0.0
  in { remlen: lineLen - maxLen, hidden: maxWord }
  where
    go :: Int -> Int -> Int -> String -> Number -> { maxWord :: String, maxLen :: Number }
    go lbound rbound needle maxWord maxLenSeen =
      if needle == lbound || needle == rbound
        then { maxWord, maxLen: maxLenSeen }
        else let
          here = String.take needle word
          hereLen = renderWord here
          newMaxLen = if hereLen <= lineLen then max hereLen maxLenSeen else maxLenSeen
          newMaxWord = if newMaxLen == hereLen then here else maxWord
        in if hereLen <= lineLen then go needle rbound ((needle + rbound) / 2 + 1) newMaxWord newMaxLen
           else go lbound needle ((needle + lbound) / 2) newMaxWord newMaxLen

-- | Calculate the longest prefix of `word' such that the prefix <> "…" fits
-- | within `lineLen'. Just like with `calcHiddenLength', the easiest way to
-- | do this is a binary search on the rendered length.
-- |
-- | Precondition: rendered length of `word' must be greater than `lineLen'.
-- | The proof of the correctness of the algorithm follows from that precondition;
-- | if the user wants truncation, any strict prefix must have a suffix of "…", which,
-- | by inspection, makes the prefixes monotonically increasing in render length as a
-- | function of prefix length. The precondition removes the necessity of considering
-- | the full word without an ellipsis, which could be shorter than the longest
-- | augmented prefix that fits.
calcTruncateLength :: RenderContext -> String -> { remlen :: Number, truncate :: String }
calcTruncateLength { renderWord, lineLen } word =
  let
    charLen = String.length word
    { maxWord, maxLen } = go 0 charLen (charLen / 2) "…" (renderWord "…")
  in { remlen: lineLen - maxLen, truncate: maxWord }
  where
    go :: Int -> Int -> Int -> String -> Number -> { maxWord :: String, maxLen :: Number }
    go lbound rbound needle maxWord maxLenSeen =
      if needle == lbound || needle == rbound
        then { maxWord, maxLen: maxLenSeen }
        else let
          here = String.take needle word <> "…"
          hereLen = renderWord here
          newMaxLen = if hereLen <= lineLen then max hereLen maxLenSeen else maxLenSeen
          newMaxWord = if newMaxLen == hereLen then here else maxWord
        in if hereLen <= lineLen then go needle rbound ((needle + rbound) / 2 + 1) newMaxWord newMaxLen
           else go lbound needle ((needle + lbound) / 2) newMaxWord newMaxLen

-- TODO: Is it worth it to write some kind of binary search helper?

-- contains-all-words <== forall word. word in X ==> word in breakLines(X)
-- no-extra-words <== forall word. word in breakLines(X) ==> word in X
-- idempotent <== forall X. breakLines(X) = breakLines(breakLines(X))
-- below length <== forall line. line in breakLines(X) ==> pixelLength(line) <= lineLen

-- But we can't test this without a browser harness! We'd need some sort of
-- headless driver to run these tests bluhhhhhh
