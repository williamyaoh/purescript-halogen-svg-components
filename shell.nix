{ sources ? import ./nix/sources.nix {} }:

let
  pkgs = import sources.nixpkgs {};
  easy-ps = import sources.easy-purescript-nix {};

in

pkgs.mkShell {
  buildInputs = [
    easy-ps.purs-0_14_1
    easy-ps.spago
    pkgs.nodejs-12_x
  ];
}
